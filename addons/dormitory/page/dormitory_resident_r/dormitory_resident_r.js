frappe.pages['dormitory-resident-r'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Resident Report',
		single_column: true
	});

	let list = $(frappe.render_template('<div class="table-area"></div>')).appendTo(page.main);
	// let srcdt = document.createElement('script');
	// srcdt.src = "https://unpkg.com/frape-datatable@latest";
	// srcdt.type = "text/javascript";
	// document.head.appendChild(srcdt);

	page.building_category_field = page.add_field({
		fieldname: 'building_category',
		label: 'Building Category',
		fieldtype:'Select',
		options: ["","Staff","Non Staff"],
		default:"",
		change: function() {
			get_data();
		}
	});
	
	page.building_field = page.add_field({
		fieldname: 'dormitory_building_id',
		label: 'Building',
		fieldtype:'Link',
		options: "Dormitory Building",
		get_query: () => {
			if (page.building_category_field.get_value()){
				return {
					filters: {
						building_category: page.building_category_field.get_value(),
					}};
			}
		},
		change: function() {
			get_data();
		}
	});

	page.resident_date_field = page.add_field({
		fieldname: 'resident_date',
		label: 'Resident Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	// page.emp_designation_cat_field = page.add_field({
	// 	fieldname: 'empdesignationcat',
	// 	label: 'Employee Designation Category',
	// 	fieldtype: 'Select',
	// 	options: ['','Staff', 'Non-Staff'],
	// 	change: function() {
	// 		get_data();
	// 	}
	// });

	page.emp_full_name_field = page.add_field({
		fieldname: 'empfullname',
		label: 'Employee Name',
		fieldtype: 'Data',
		change: function() {
			get_data();
		}
	});

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_datatable_dormitory_resident_report',
			args: {
				building_cat:page.building_category_field.get_value(),
				building:page.building_field.get_value(),
				tgl:page.resident_date_field.get_value(),
				// emp_designation_cat:page.emp_designation_cat_field.get_value(),
				emp_full_name:page.emp_full_name_field.get_value()
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					console.log(r);
					let result = r.message;
					make_table(result);
				}
			}
		});
	}

	// function make_table(data){
	// 	let datatabel = new DataTable('#list-dormitory-resident',{
	// 		columns: data.header,
	// 		data: data.content,
	// 	});


	// }

	function make_table(data){
		list.html("");
		$(frappe.render_template('dormitory_resident_r',{data:data})).appendTo(list);

	}

	// $(frappe.render_template('dormitory_resident_r_header')).appendTo(page.main);
	
	// frappe.call({
	// 	method:'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_all_toolkit_item',
	// 	callback: function(r) {

	// 		$(frappe.render_template('dormitory_resident_r_header')).appendTo(page.main);
	// 		// if(!r.exc) {
	// 			// r.render();
	// 		// }
	// 		console.log(r);
	// 	}
	// });
	
	// frappe.call({
	// 	method:'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_dormitory_resident_report_data',
	// 	callback: function(r) {
	// 		$(frappe.render_template('dormitory_resident_r'));
	// 		// if(!r.exc) {
	// 			this.render();
	// 		// }
	// 		console.log(r);
	// 	}
	// });
	

	// wrapper.dormitory_resident_r = new addons.DormitoryResidentR(wrapper);
		

	// parent.html(frappe.render_template("dormitory_resident_r_header").appendTo(page.main));

	// frappe.require('item-dashboard.bundle.js', function() {
		
		
		// $(frappe.render_template('dormitory_resident_r'));


		
		// page.dormitory_resident_r = new addons.DormitoryResidentR({
		// 	page_name: "dormitory-resident",
		// 	page_length: 10,
		// 	parent: page.main,
		// 	method: 'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_dormitory_resident_report_data',
		// 	template: 'dormitory_resident_r'
		// });

	// });


	

}

	

	// addons.DormitoryResidentR = class DormitoryResidentR{
	// 	constructor(wrapper) {
	// 		$.extend(this, wrapper);
	// 		var me = this;
	// 		// 0 setTimeout hack - this gives time for canvas to get width and height
	// 		// setTimeout(function() {
	// 			// me.setup(wrapper);
	// 			// me.get_header();
	// 			// me.get_data();
	// 		// }, 0);
	// 	}

	// 	setup(){
	// 		this.content = $(frappe.render_template('dormitory_resident_r')).appendTo(this.parent);
	// 	}

		

	// 	get_header(){
	// 		$(frappe.render_template('dormitory_resident_r_header')).appendTo(page.main);
	
	// 		frappe.call({
	// 			method:'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_all_toolkit_item',
	// 			callback: function(r) {
	// 				// if(!r.exc) {
	// 					// this.render();
	// 				// }
	// 				console.log(r);
	// 			}
	// 		});
	// 	}
	
	// 	get_data(){
	// 		frappe.call({
	// 			method:'addons.dormitory.page.dormitory_resident_r.dormitory_resident_r.get_dormitory_resident_report_data',
	// 			callback: function(r) {
	// 				// if(!r.exc) {
	// 					this.render();
	// 				// }
	// 				console.log(r);
	// 			}
	// 		});
	// 	}

		
	// }



	