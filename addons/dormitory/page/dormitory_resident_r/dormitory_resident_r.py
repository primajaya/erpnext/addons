import frappe
from datetime import date

@frappe.whitelist()
def get_dormitory_resident_report_data():
    # values = {'company': 'Frappe Technologies Inc'}
    data = frappe.db.sql("""
        select 
            a.name as Building_id,
            a.building_name,
            a.building_address,
            b.name as room_id,
            b.dormitory_room_name,
            b.dormitory_room_capacity

        from `tabDormitory Building` as a
        join 
        `tabDormitory Room` as b ON a.name=b.dormitory_building_id
    """, 
    # values=values, 
    as_dict=1)
    return data

@frappe.whitelist()
def get_all_toolkit_item():
    data = frappe.db.get_list("Dormitory Tool Kit Item", pluck='tool_kit_name')
    return data

@frappe.whitelist()
def get_datatable_dormitory_resident_report(
      building_cat=None,
      building=None, 
      tgl=None, 
    #   emp_designation_cat=None, 
      emp_full_name=None):
    data = {'header':['Building','Room','Capacity','Employee Name','NRP','Mobile','Position','Department']}
    
    # headers = frappe.db.get_list("Dormitory Tool Kit Item", pluck='tool_kit_name')
	# data['header'] +=headers

    tabDormitoryToolKitItem = frappe.qb.DocType("Dormitory Tool Kit Item")
	
    # headers = frappe.db.get_list("Dormitory Tool Kit Item")
    # headers = frappe.db.sql(""" select * from `tabDormitory Tool Kit Item`""", as_dict=True)
	
    headers =(
          frappe.qb.from_(tabDormitoryToolKitItem).as_("dtki")
          .select(
                tabDormitoryToolKitItem.name,
                tabDormitoryToolKitItem.tool_kit_name,
                # fieldheader
            )
          ).run(as_dict=True)
    

    where = """ where 1=1 """
    values = {}
    groupBy = """ """
    

    field = """b.building_name
            ,r.dormitory_room_name
            ,r.dormitory_room_capacity
            , (case when res.employee_name is null then '' else res.employee_name end ) as employee_name 
            , (case when res.employee_nrp is null then '' else res.employee_nrp end ) as employee_nrp 
            , (case when res.employee_cell_number is null then '' else res.employee_cell_number end ) as employee_cell_number 
            , (case when res.employee_posision is null then '' else res.employee_posision end ) as employee_posision 
            , (case when res.employee_department is null then '' else res.employee_department end ) as employee_department 
            """
    fromtbl = """ from `tabDormitory Building` as b """
    joining = """ join 
            `tabDormitory Room` as r ON b.name=r.dormitory_building_id
        
            """
    joining += """ 
            left join 
                ( SELECT 
                    max(name) as name, 
                    dormitory_building_id,
                    dormitory_room_id,
                    type_log,
                    resident_date,
                    description,
                    employee_id,
                    employee_name,
                    employee_nrp,
                    employee_cell_number,
                    employee_posision,
                    employee_department
                FROM `tabDormitory Resident` 
                GROUP BY resident_date, employee_id 
                ORDER BY name DESC)
            AS res ON r.name=res.dormitory_room_id AND res.type_log='IN'
            """
    # if tgl:
    values['tgl'] = tgl if tgl else date.today()
    joining +=""" and res.resident_date=%(tgl)s """

    if building_cat:
        values['building_cat'] =building_cat
        where +=""" and b.building_category=%(building_cat)s """

    if building:
        values['building'] =building
        where +=""" and b.name=%(building)s """
    
    # if emp_designation_cat:
    #     joining +=""" left join 
    #     `tabDesignation` as d ON d.name=res.employee_id """
    #     values['emp_designation_cat'] =emp_designation_cat
    #     where +=""" and d.category = %(emp_designation_cat)s """
          
    if emp_full_name:
        values['emp_full_name'] ="%"+emp_full_name+"%"
        where +=""" and res.employee_name like %(emp_full_name)s """

    
    
    for header in headers:
        (data['header']).append(header.tool_kit_name)
        # data['header'] = header
        id =header.name 
        tblas = "tl_"+id
        fieldas = "field_"+id
        field +=""" ,(
            case 
                when """+tblas+""".dormitory_quantity is null then 0 
                else """+tblas+""".dormitory_quantity
            end) as """+fieldas
        # if header. # jika bukan building cat tool kit itemnya
        joining +="""
            left join 
                `tabDormitory Tool Kit` as """+tblas+""" ON (r.name="""+tblas+""".dormitory_room_id or ("""+tblas+""".dormitory_room_id is null and """+tblas+""".dormitory_building_id = r.dormitory_building_id)) and """+tblas+""".dormitory_tool_kit_item_id='"""+id+"""' 
            
            """
        
    # where +=""" and res.name in ( """+subq+""" )"""
    
    content = frappe.db.sql( query="""
        select """+field+"""
            
        """+fromtbl+joining+where+groupBy+"""

        order by b.building_name, r.dormitory_room_name ASC
    """ 
    ,values=values
    ,as_dict=False
    )

    

    foot = []
    idxcount=2
    for i,vals in enumerate(content):
        for index,val in enumerate(vals):
            if index>=idxcount:
                if index==3 and val:
                     val=1
                elif index==3 and not val:
                     val=0
                
                if index==4 or index==5 or index==6 or index==7 :
                    val=""

                if i==0:
                    foot.append(val)
                else:
                    isi = foot[index-idxcount]
                    if index==2 and vals[0]==(content[i-1])[0] and vals[1]==(content[i-1])[1] :
                         isi +=0
                    else:
                        isi +=val

                    foot[index-idxcount] = isi

    if len(foot) :
         foot[1] = "filled = "+str(foot[1]) +", \n available = "+str(foot[0]-foot[1])

    dataContent = []
    buildingRowspan = 0
    roomRowspan = 0
    capacityRowspan = 0
    for i,vals in enumerate(content):
        dataRow = []
        for index,val in enumerate(vals):
            if i==0:
                dataRow.append({'data':val,'rowspan':0})
            else:
                if index==0 and vals[0]==(content[i-1])[0]:
                    rowspan = 2 if ((dataContent[i-1])[0])['rowspan']==0 else ((dataContent[i-1])[0])['rowspan']+1
                    dataRow.append({'data':'null', 'rowspan':rowspan})
                    (dataContent[i-(rowspan-1)])[0] = {'data':val, 'rowspan':rowspan}
                elif index==0 and vals[0]!=(content[i-1])[0]:
                    dataRow.append({'data':val,'rowspan':0})
                
                if index==1 and vals[1]==(content[i-1])[1]:
                    rowspan = 2 if ((dataContent[i-1])[1])['rowspan']==0 else ((dataContent[i-1])[1])['rowspan']+1
                    dataRow.append({'data':'null', 'rowspan':rowspan})
                    (dataContent[i-(rowspan-1)])[1] = {'data':val, 'rowspan':rowspan}
                elif index==1 and vals[1]!=(content[i-1])[1]:
                    dataRow.append({'data':val,'rowspan':0})

                if index==2 and vals[1]==(content[i-1])[1] and vals[2]==(content[i-1])[2] :
                    rowspan = 2 if ((dataContent[i-1])[2])['rowspan']==0 else ((dataContent[i-1])[2])['rowspan']+1
                    dataRow.append({'data':'null', 'rowspan':rowspan})
                    (dataContent[i-(rowspan-1)])[2] = {'data':val, 'rowspan':rowspan}
                elif index==2 and vals[1]!=(content[i-1])[1]: # and vals[2]!=(content[i-1])[2]:
                    dataRow.append({'data':val,'rowspan':0})
                
                if index >2 :
                    dataRow.append({'data':val,'rowspan':0})
        
        dataContent.append(dataRow)
                    

    data['content'] = content
    data['datacontent'] = dataContent
    data['footer'] = foot

    return data

def get_base_salary(nik,branch,designation,joining_date,start_date):
	total_day = date_diff(start_date, joining_date)
	if total_day < 0:
		frappe.throw(_("Joining Date greather than Start Date Payroll"))
	# frappe.throw(_(nik + " = " + str(total_day)))
	tabCustomParent = frappe.qb.DocType("Employee Salary Custom")
	tabCustomDetail = frappe.qb.DocType("Employee Salary Custom Detail")
	custom_salaries = (
		frappe.qb.from_(tabCustomParent).as_("p")
		.inner_join(tabCustomDetail)
		.on(tabCustomDetail.parent == tabCustomParent.name).as_("d")
		.select(
			tabCustomDetail.base_salary,
			tabCustomDetail.field_salary,
			tabCustomDetail.allowance_salary,
			tabCustomDetail.overtime_salary
		)
		.where(((tabCustomDetail.start_year * 365) <= total_day) 
			& ((tabCustomDetail.end_year * 365) > total_day )
			& (tabCustomParent.employee == nik )
		)
		.limit(1)

	).run(as_dict=True)

	base_salary = 0
	if len(custom_salaries) > 0:
		base_salary = custom_salaries[0].base_salary
		return base_salary
		
	tabParent = frappe.qb.DocType("Group Designation Salary")
	tabItem = frappe.qb.DocType("Group Designation Salary Item")
	tabDetail = frappe.qb.DocType("Group Designation Salary Detail")
	salaries = (
		frappe.qb.from_(tabParent).as_("p")
		.inner_join(tabItem)
		.on(tabItem.parent == tabParent.name).as_("i")
		.inner_join(tabDetail)
		.on(tabDetail.parent == tabParent.name).as_("d")
		.select(
			tabDetail.base_salary,
			tabDetail.field_salary,
			tabDetail.allowance_salary,
			tabDetail.overtime_salary,
			tabItem.designation
		)
		.where(((tabDetail.start_year * 365) <= total_day) 
			& ((tabDetail.end_year * 365) > total_day )
			& (tabItem.designation == designation )
		)
		.limit(1)

	).run(as_dict=True)
	# frappe.throw(_("Designation from query {0}").format(salaries[0].designation))
	base_salary = 0
	if len(salaries) > 0:
		base_salary = salaries[0].base_salary 

	return base_salary