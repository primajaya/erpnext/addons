frappe.pages['dormitory-building-i'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Dormitory Inspection',
		single_column: true
	});

	let list = $(frappe.render_template('<div id="list-dormitory-inspection" class="table-area"></div>')).appendTo(page.main);
	// let srcdt = document.createElement('script');
	// srcdt.src = "https://unpkg.com/frape-datatable@latest";
	// srcdt.type = "text/javascript";
	// document.head.appendChild(srcdt);
	// $('<script src="https://unpkg.com/frappe-datatable@latest"></script>').appendTo(document.head);

	// $('<script src="https://code.jquery.com/jquery-3.7.0.js"></script>').appendTo(document.head);
	// $('<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>').appendTo(document.head);

	// page.inspection_category_field = page.add_field({
	// 	fieldname: 'category',
	// 	label: 'Inspection Category',
	// 	fieldtype:'Link',
	// 	options:"Dormitory Parameter Category",
	// 	change: function() {
	// 		get_data();
	// 	}
	// });

	// page.inspection_date_field = page.add_field({
	// 	fieldname: 'inspection_date',
	// 	label: 'inspection Date',
	// 	fieldtype:'Date',
	// 	default: frappe.datetime.get_today(),
	// 	change: function() {
	// 		get_data();
	// 	}
	// });

	// page.category_building_field = page.add_field({
	// 	fieldname: 'category_building',
	// 	label: 'Category Building',
	// 	fieldtype:'Select',
	// 	default: "All",
	// 	options: ["All","Staff", "Non Staff"],
	// 	change: function() {
	// 		get_data();
	// 	}
	// });

	page.inspection_parameter_field = page.add_field({
		fieldname: 'parameter',
		label: 'Inspection Paramater',
		fieldtype:'Link',
		options:"Dormitory Inspection Parameter",
		change: function() {
			// add_filter(this.value);
			get_data();
		}
	});
	page.inspection_date_field = page.add_field({
		fieldname: 'inspection_date',
		label: 'inspection Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});
	page.category_building_field = page.add_field({
		fieldname: 'category_building',
		label: 'Category Building',
		fieldtype:'Select',
		default: "All",
		options: ["All","Staff", "Non Staff"],
		hidden: 0,
		change: function() {
			get_data();
		}
	});

	function add_filter(data){
		page.category_building_field.df.hidden=0;
		page.refresh_field("category_building_field");
		console.log("============");
		console.log(page.category_building_field);
		// page.set_df_property("category_building_field","hidden",0);
		// page.category_building_field.set_df_property("hidden",1)
	}

	// function get_data(){
	// 	let me = this
	// 	frappe.call({
	// 		method:'addons.dormitory.page.dormitory_building_i.dormitory_building_i.get_datatable_dormitory_building_inspection_report',
	// 		args: {
	// 			tgl: page.inspection_date_field.get_value(),
	// 			category: page.inspection_category_field.get_value(),
	// 			building_cat: page.category_building_field.get_value()
	// 		},
	// 		callback: function(r) {
	
	// 			if(!r.exc) {
					
	// 				// console.log(r);
	// 				let result = r.message;
	// 				// make_datatable(result);
	// 				make_table(result);
	// 			}
	// 		}
	// 	});
	// }

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.dormitory.page.dormitory_building_i.dormitory_building_i.get_dormitory_building_inspection_report',
			args: {
				parameter: page.inspection_parameter_field.get_value(),
				tgl: page.inspection_date_field.get_value(),
				building_cat: page.category_building_field.get_value()
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					// console.log(r);
					let result = r.message;
					// make_datatable(result);
					make_table(result);
				}
			}
		});
	}

	function make_datatable(data){
		const datatable = new DataTable('#list-dormitory-inspection',{
			columns: data.header,
			
			data: data.content,
			
		});

		// datatable.datamanager.sortRows(0, 'none');
	}

	function make_table(data){
		console.log(data);
		list.html("");
		$(frappe.render_template("dormitory_inspection_table",{data:data})).appendTo(list);
	}

}