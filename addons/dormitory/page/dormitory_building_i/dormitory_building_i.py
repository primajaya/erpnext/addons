from addons.utils.logger import get_logger
import frappe
from frappe import _

log = get_logger(__name__)

@frappe.whitelist()
def get_datatable_dormitory_building_inspection_report(tgl, category, building_cat="All"):
    
    log.info("test")
    rowspanHeader = 3
    
    parameterCategoryType = frappe.db.get_value("Dormitory Parameter Category",category,"parameter_category_type",as_dict=False)
    if not parameterCategoryType:
        parameterCategoryType = "Room"

    print("parameterCategoryType")
    print(parameterCategoryType)
    queryHeaderWhere = """ where 1=1 """

    valuesHeader = {}
    if building_cat != "All":
        queryHeaderWhere +=""" and db.building_category=%(building_cat)s """
        valuesHeader['building_cat'] = building_cat

    queryHeader="""
            select 
            dr.name as id,
            db.name as building_id,
            db.building_name,
            db.building_category,
            dr.name as room_id,
            dr.dormitory_room_name
            from `tabDormitory Room` as dr
            left join `tabDormitory Building` as db ON db.name=dr.dormitory_building_id 
            """+queryHeaderWhere+"""
            order By db.building_category DESC, db.building_name ASC, dr.dormitory_room_name ASC
        """
    if parameterCategoryType == "Building":
        queryHeader=""" select db.name as id,db.name as building_id,db.building_name,db.building_category from `tabDormitory Building` as db
            """+queryHeaderWhere+"""
            order By db.building_category DESC, db.building_name ASC """
        rowspanHeader=2

    headers = frappe.db.sql(queryHeader,values=valuesHeader,as_dict=True)
    

    data = {
        'header':[
            # {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Point','rowspan':rowspanHeader,'colspan':0}
            ],
        'content':'',
        'footer':{'footone':[],'foottwo':[]}
        }
    
    building_category_list_sql = """select count(*) as jml
            , db.building_category 
            from `tabDormitory Room` tdr
            left join `tabDormitory Building` db on db.name = tdr.dormitory_building_id 
            """+queryHeaderWhere+""" 
            group by db.building_category 
            order by db.building_category DESC"""
    
    if parameterCategoryType == "Building":
        building_category_list_sql = """select count(*) as jml
            , db.building_category 
            from `tabDormitory Building` as db 
            """+queryHeaderWhere+""" 
            group by db.building_category 
            order by db.building_category DESC"""

    print(building_category_list_sql)
    building_category_list = frappe.db.sql(building_category_list_sql,values=valuesHeader, as_dict=True)

    tableHeader1 = [
        {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
        {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
        {'name':'Point','rowspan':rowspanHeader,'colspan':0}]
    for building_cat in building_category_list:
        tableHeader1.append({'name':building_cat.building_category,'rowspan':0,'colspan':building_cat.jml})
    
    (data['header']).append(tableHeader1)

    tableHeader2 = [
        # {'name':'','rowspan':0,'colspan':0}, 
        # {'name':'','rowspan':0,'colspan':0}, 
        # {'name':'','rowspan':0,'colspan':0}
        ]
    tableHeader3 = tableHeader2.copy()
    if parameterCategoryType == "Room":
        mess_count_room = frappe.db.sql("""select count(*) as jml
            , db.building_name 
            , tdr.dormitory_building_id 
            from `tabDormitory Room` tdr
            left join `tabDormitory Building` db on db.name = tdr.dormitory_building_id 
            """+queryHeaderWhere+""" 
            group by  tdr.dormitory_building_id 
            order by db.building_category DESC,db.building_name ASC""",values=valuesHeader, as_dict=True
        )
        for mess_count in mess_count_room:
            tableHeader2.append({'name':mess_count.building_name,'rowspan':0,'colspan':mess_count.jml})
    
        (data['header']).append(tableHeader2)

    
    
    where = """ where 1=1 """
    values = {}

    field = """ dp.dormitory_parameter_name
            ,dpi.parameter_item_name, dpi.parameter_item_point
            # ,dipf.dormitory_inspection_value
              """
    fromtbl = """ from `tabDormitory Parameter Category` as dpc """
    joining = """ 
            join 
                `tabDormitory Parameter` as dp ON dpc.name=dp.dormitory_parameter_category_id

            join
                `tabDormitory Parameter Item` as dpi ON dp.name=dpi.parent

            # left join 
            #     `tabDormitory Inspection Parameter Form` as  dipf ON 
            #     dipf.dormitory_parameter_item_id=dpi.name and 
            #     dipf.dormitory_parameter_cat_id=dp.name
                
            """
    
    where +=""" and dp.dormitory_parameter_category_id=%(category)s"""
    values['category'] = category
    values['tgl'] = tgl

    # if tgl:
    #       joining += """ 
    #             left join 
    #                 `tabDormitory Resident` as res On r.name=res.dormitory_room_id and res.type_log='IN'
    #             """
    #       values['tgl'] =tgl
    #       joining +=""" and res.time=%(tgl)s """
    # else:
    #       joining += """ 
    #             left join 
    #                 `tabDormitory Resident` as res On r.name=res.dormitory_room_id and res.type_log='IN'
    #             """

    
    for header in headers:

        
        if parameterCategoryType == "Building":
            header['name'] = header.building_name
        else:
            header['name'] = header.dormitory_room_name
        header['colspan'] =0
        header['rowspan'] =0
        tableHeader3.append(header)
        # (data['header']).append(header)
        # data['header'] = header
        id =header.id 
        tblas = "tl_"+id
        fieldas = "field_"+id
        building_idas = 'building_id_'+id
        room_idas = 'room_id_'+id
        field +=""" ,(
            case 
                when """+tblas+""".dormitory_inspection_value is null then 0 
                else """+tblas+""".dormitory_inspection_value
            end) as """+fieldas
        # if header. # jika bukan parameter category type 'Room'
        
        joining +="""
            left join 
                `tabDormitory Inspection Parameter Form` as """+tblas+""" ON """+tblas+""".dormitory_parameter_item_id=dpi.name 
                and """+tblas+""".dormitory_inspection_date =%(tgl)s 
                and """+tblas+""".dormitory_building_id =%("""+building_idas+""")s """
        
        values[building_idas] = header.building_id

        if parameterCategoryType == "Room":
            joining +=""" and """+tblas+""".dormitory_room_id= %("""+room_idas+""")s """
            values[room_idas] = header.room_id
        

    (data['header']).append(tableHeader3)
        

    print(""" select """+field+"""
        """+fromtbl+joining+where+"""
        order by dp.dormitory_parameter_name ASC """)
    
    content = frappe.db.sql( query="""
        select """+field+"""
            
        """+fromtbl+joining+where+"""

        order by dp.dormitory_parameter_name ASC
    """ 
    ,values=values
    ,as_dict=False
    # ,as_dict=True
    )
    
    data['content'] = content

    foot1 = []
    foot2 = []
    idxcount=2
    for i,vals in enumerate(content):
        for index,val in enumerate(vals):
            if index>=idxcount:
                if i==0:
                    foot1.append(val)
                elif len(foot1)>0:
                    isi = foot1[index-idxcount]
                    isi +=val
                    foot1[index-idxcount] = isi

    for val in foot1:
        foot2.append((val/foot1[0])*100)

    (data['footer'])['footone'] = foot1
    (data['footer'])['foottwo'] = foot2

    return data


@frappe.whitelist()
def get_dormitory_building_inspection_report(parameter, tgl, building_cat="All"):
    
    log.info("test")
    rowspanHeader = 3
    data = {
        'header':[
            # {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Point','rowspan':rowspanHeader,'colspan':0}
            ],
        'content':'',
        'footer':{'footone':[],'foottwo':[]}
        }
    
    parameterType = "Room"
    parameter_inspection = frappe.db.get_value("Dormitory Inspection Parameter",parameter,"*",as_dict=True)
    log.info("parameter_inspection")
    log.info(parameter_inspection)
    if not parameter_inspection:
        return data
        
    parameterType = parameter_inspection.dor_insp_param_custom_header
    
    
    # parameterCategoryType = frappe.db.get_value("Dormitory Parameter Category",category,"parameter_category_type",as_dict=False)
    # if not parameterCategoryType:
    #     parameterCategoryType = "Room"

    queryHeaderWhere = """ where 1=1 """
    valuesHeader = {}
    
    if(parameterType!="Custom"):
        
        if building_cat != "All":
            queryHeaderWhere +=""" and db.building_category=%(building_cat)s """
            valuesHeader['building_cat'] = building_cat

        queryHeader="""
                select 
                dr.name as id,
                db.name as building_id,
                db.building_name,
                db.building_category,
                dr.name as room_id,
                dr.dormitory_room_name
                from `tabDormitory Room` as dr
                left join `tabDormitory Building` as db ON db.name=dr.dormitory_building_id 
                """+queryHeaderWhere+"""
                order By db.building_category DESC, db.building_name ASC, dr.dormitory_room_name ASC
            """
        if parameterType == "Building":
            queryHeader=""" select db.name as id,db.name as building_id,db.building_name,db.building_category from `tabDormitory Building` as db
                """+queryHeaderWhere+"""
                order By db.building_category DESC, db.building_name ASC """
            rowspanHeader=2
    
    else:
        queryHeaderWhere += """ and parent=%(parameter)s """
        valuesHeader['parameter'] = parameter
        
        queryHeader = """ select *
                    from `tabDormitory Inspection Parameter Custom Header`
                    """+ queryHeaderWhere +""" 
                    order by idx ASC """
        
    headers = frappe.db.sql(queryHeader,values=valuesHeader,as_dict=True)
    
    log.info("headers")
    log.info(headers)

    # data = {
    #     'header':[
    #         # {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
    #         # {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
    #         # {'name':'Point','rowspan':rowspanHeader,'colspan':0}
    #         ],
    #     'content':'',
    #     'footer':{'footone':[],'foottwo':[]}
    #     }
    
    tableHeader1 = [
            {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
            {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
            {'name':'Point','rowspan':rowspanHeader,'colspan':0}]
    tableHeader2 = [
            # {'name':'','rowspan':0,'colspan':0}, 
            # {'name':'','rowspan':0,'colspan':0}, 
            # {'name':'','rowspan':0,'colspan':0}
            ]
    tableHeader3 = tableHeader2.copy()
    
    if(parameterType!="Custom"):
    
        building_category_list_sql = """select count(*) as jml
                , db.building_category 
                from `tabDormitory Room` tdr
                left join `tabDormitory Building` db on db.name = tdr.dormitory_building_id 
                """+queryHeaderWhere+""" 
                group by db.building_category 
                order by db.building_category DESC"""
        
        if parameterType == "Building":
            building_category_list_sql = """select count(*) as jml
                , db.building_category 
                from `tabDormitory Building` as db 
                """+queryHeaderWhere+""" 
                group by db.building_category 
                order by db.building_category DESC"""

        print(building_category_list_sql)
        building_category_list = frappe.db.sql(building_category_list_sql,values=valuesHeader, as_dict=True)

        
        for building_cat in building_category_list:
            tableHeader1.append({'name':building_cat.building_category,'rowspan':0,'colspan':building_cat.jml})
        
        (data['header']).append(tableHeader1)

        
        if parameterType == "Room":
            mess_count_room = frappe.db.sql("""select count(*) as jml
                , db.building_name 
                , tdr.dormitory_building_id 
                from `tabDormitory Room` tdr
                left join `tabDormitory Building` db on db.name = tdr.dormitory_building_id 
                """+queryHeaderWhere+""" 
                group by  tdr.dormitory_building_id 
                order by db.building_category DESC,db.building_name ASC""",values=valuesHeader, as_dict=True
            )
            for mess_count in mess_count_room:
                tableHeader2.append({'name':mess_count.building_name,'rowspan':0,'colspan':mess_count.jml})
        
            (data['header']).append(tableHeader2)

    else:
         (data['header']).append(tableHeader1)
        
    
    where = """ where 1=1 """
    values = {}
    
    field = """ 
            dipd.dor_insp_det_param
            ,(case 
                when dipd.dor_insp_det_sub_param is null then ''
                else dipd.dor_insp_det_sub_param
            end) as dor_insp_det_sub_param
            ,(case 
                when dipd.dor_insp_det_point is null then 0 #change this with point field
                else dipd.dor_insp_det_point
            end) as dor_insp_det_point
            """
    fromtbl = """ from `tabDormitory Inspection Parameter Detail` as dipd """

    # field = """ dp.dormitory_parameter_name
    #         ,dpi.parameter_item_name, dpi.parameter_item_point
    #         # ,dipf.dormitory_inspection_value
    #           """
    # fromtbl = """ from `tabDormitory Parameter Category` as dpc """
    joining = """ """
            # join 
            #     `tabDormitory Parameter` as dp ON dpc.name=dp.dormitory_parameter_category_id

            # join
            #     `tabDormitory Parameter Item` as dpi ON dp.name=dpi.parent

            # # left join 
            # #     `tabDormitory Inspection Parameter Form` as  dipf ON 
            # #     dipf.dormitory_parameter_item_id=dpi.name and 
            # #     dipf.dormitory_parameter_cat_id=dp.name
                
            # """
    
    where +=""" and dipd.parent=%(parameter)s"""
    values['parameter'] = parameter
    values['tgl'] = tgl

    # if tgl:
    #       joining += """ 
    #             left join 
    #                 `tabDormitory Resident` as res On r.name=res.dormitory_room_id and res.type_log='IN'
    #             """
    #       values['tgl'] =tgl
    #       joining +=""" and res.time=%(tgl)s """
    # else:
    #       joining += """ 
    #             left join 
    #                 `tabDormitory Resident` as res On r.name=res.dormitory_room_id and res.type_log='IN'
    #             """

    
    for header in headers:

        match parameterType:
            case "Custom":
                header['id'] = header.name
                header['name'] = header.dor_insp_param_custom_header_name
            case "Building":
                header['name'] = header.building_name
            case _: #Room
                header['name'] = header.dormitory_room_name
                
        header['colspan'] =0
        header['rowspan'] =0
        
        tableHeader3.append(header)
        # (data['header']).append(header)
        # data['header'] = header
        id =header.id 
        log.info("id")
        log.info(id)
        tblas = "tl_"+id
        fieldas = "field_"+id
        header_custom = "custom_"+id
        building_idas = 'building_id_'+id
        room_idas = 'room_id_'+id
        field +=""" ,(
            case 
                when """+tblas+""".dormitory_insp_value is null then 0 
                else """+tblas+""".dormitory_insp_value
            end) as """+fieldas
        # if header. # jika bukan parameter category type 'Room'
        
        joining +="""
            left join 
                `tabDormitory Inspection Detail` as """+tblas+""" ON """+tblas+""".dormitory_insp_parameter_detail_id=dipd.name 
                and """+tblas+""".dormitory_insp_date =%(tgl)s """
                
        if parameterType == "Building" or parameterType == "Room":
            joining +="""and """+tblas+""".dormitory_building_id =%("""+building_idas+""")s """
            values[building_idas] = header.building_id

        if parameterType == "Room":
            joining +=""" and """+tblas+""".dormitory_room_id= %("""+room_idas+""")s """
            values[room_idas] = header.room_id
        
        if parameterType == "Custom":
            joining +=""" and """+tblas+""".dormitory_insp_detail_header_id= %("""+header_custom+""")s """
            values[header_custom] = header.id
        

    (data['header']).append(tableHeader3)
        

    print(""" select """+field+"""
        """+fromtbl+joining+where+"""
        order by dipd.idx ASC """)
    
    content = frappe.db.sql( query="""
        select """+field+"""
            
        """+fromtbl+joining+where+"""

        order by dipd.idx ASC
    """ 
    ,values=values
    ,as_dict=False
    # ,as_dict=True
    )
    
    data['content'] = content

    foot1 = []
    foot2 = []
    idxcount=2
    for i,vals in enumerate(content):
        for index,val in enumerate(vals):
            # val = int(val)
            if index>=idxcount:
                if i==0:
                    foot1.append(int(val))
                elif len(foot1)>0:
                    isi = int(foot1[index-idxcount])
                    isi +=int(val)
                    foot1[index-idxcount] = isi

    for val in foot1:
        if int(foot1[0])==0:
            foot2.append(0*100)
        else:
            foot2.append((val/foot1[0])*100)

    (data['footer'])['footone'] = foot1
    (data['footer'])['foottwo'] = foot2

    return data