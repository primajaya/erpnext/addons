frappe.pages['dormitory_resident_d'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Dormitory Resident By Date Range',
		single_column: true
	});

	let list = $(frappe.render_template('<div class="table-area"></div>')).appendTo(page.main);

	page.building_category_field = page.add_field({
		fieldname: 'building_category',
		label: 'Building Category',
		fieldtype:'Select',
		options: ["","Staff","Non Staff"],
		default:"",
		change: function() {
			get_data();
		}
	});
	
	page.building_field = page.add_field({
		fieldname: 'dormitory_building_id',
		label: 'Building',
		fieldtype:'Link',
		options: "Dormitory Building",
		get_query: () => {
			if (page.building_category_field.get_value()){
				return {
					filters: {
						building_category: page.building_category_field.get_value(),
					}};
			}
		},
		change: function() {
			get_data();
		}
	});

	page.resident_start_date_field = page.add_field({
		fieldname: 'resident_start_date',
		label: 'Resident Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	page.resident_end_date_field = page.add_field({
		fieldname: 'resident_end_date',
		label: 'Resident Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.dormitory.page.dormitory_resident_d.dormitory_resident_d.get_datatable_dormitory_resident_report_by_date_range',
			args: {
				building_cat:page.building_category_field.get_value(),
				building:page.building_field.get_value(),
				start_date:page.resident_start_date_field.get_value(),
				end_date:page.resident_end_date_field.get_value()
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					make_table(r.message);
				}
			}
		});
	}

	
	function make_table(data){
		list.html("");
		$(frappe.render_template("dormitory_resident_by_date",{data:data})).appendTo(list);
	}

}