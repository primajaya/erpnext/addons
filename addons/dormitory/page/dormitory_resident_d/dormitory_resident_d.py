import frappe
from frappe import _
from datetime import date, datetime, timedelta

@frappe.whitelist()
def get_datatable_dormitory_resident_report_by_date_range(
      building_cat=None,
      building=None, 
      start_date=date.today(),
      end_date=date.today(),
      ):
    if isinstance(start_date, str):
        start_date = datetime.strptime(start_date,'%Y-%m-%d').date()
    if isinstance(end_date, str):
        end_date = datetime.strptime(end_date,'%Y-%m-%d').date()

    if(end_date < start_date):
        frappe.throw(_("End date cant below start date"))

    # if ((end_date-start_date).days+1) >10 :
    #     frappe.throw(_("Range date cant more then 10 days"))
    
    data = {'header':['Building','Room','Capacity','Employee Name','NRP','Mobile','Position','Department']}
    
    # headers = frappe.db.get_list("Dormitory Tool Kit Item", pluck='tool_kit_name')
	# data['header'] +=headers

    tabDormitoryToolKitItem = frappe.qb.DocType("Dormitory Tool Kit Item")
	
    # headers = frappe.db.get_list("Dormitory Tool Kit Item")
    # headers = frappe.db.sql(""" select * from `tabDormitory Tool Kit Item`""", as_dict=True)


    headers =  [start_date + timedelta(days=x) for x in range((end_date-start_date).days+1)]
    

    where = """ where 1=1 """
    values = {}
    groupBy = """ """
    

    field = """b.building_name
            ,r.dormitory_room_name
            ,r.dormitory_room_capacity
            , (case when res.employee_name is null then '' else res.employee_name end ) as emp_name
            , (case when res.employee_nrp is null then '' else res.employee_nrp end ) as employee_nrp 
            , (case when res.employee_cell_number is null then '' else res.employee_cell_number end ) as employee_cell_number 
            , (case when res.employee_posision is null then '' else res.employee_posision end ) as employee_posision 
            , (case when res.employee_department is null then '' else res.employee_department end ) as employee_department 
            """
    fromtbl = """ from `tabDormitory Building` as b """
    joining = """ join 
            `tabDormitory Room` as r ON b.name=r.dormitory_building_id
        
            """
    joining += """ 
            left join 
                ( SELECT *
                FROM `tabDormitory Resident` 
                WHERE resident_date between %(start_date)s and %(end_date)s 
                GROUP BY dormitory_room_id, employee_id 
                ORDER BY name DESC)
            AS res ON r.name=res.dormitory_room_id AND res.type_log='IN'
            """
    # # # if tgl:
    values['start_date'] = start_date.strftime('%Y-%m-%d') 
    values['end_date'] = end_date.strftime('%Y-%m-%d')
    # joining +=""" and res.resident_date between %(start_date)s and %(end_date)s """

    # joining +=""" left join `tabEmployee` as te On te.name=res.employee_id """

    if building_cat:
        values['building_cat'] =building_cat
        where +=""" and b.building_category=%(building_cat)s """

    if building:
        values['building'] =building
        where +=""" and b.name=%(building)s """
    
   
    
    for header in headers:
        id =header.strftime('%Y_%m_%d')
        (data['header']).append(id)
        # data['header'] = header
        
        tblas = "tl_"+id
        fieldas = "field_"+id
        field +=""" ,(
            case 
                when """+tblas+""".type_log = 'IN' then 1
                else 0
            end) as """+fieldas
        # # if header. # jika bukan building cat tool kit itemnya
        joining +="""
            left join (
                    SELECT MAX(type_log) as type_log, employee_id, employee_name, resident_date, dormitory_room_id, dormitory_building_id
                    FROM `tabDormitory Resident` 
                    WHERE resident_date between %(start_date)s and %(end_date)s 
                    GROUP BY resident_date, employee_id 
                    ORDER BY resident_date DESC 
                    )
                #`tabDormitory Resident`
                as """+tblas+""" ON 
                (r.name="""+tblas+""".dormitory_room_id or ("""+tblas+""".dormitory_room_id is null and """+tblas+""".dormitory_building_id = r.dormitory_building_id)) 
                and """+tblas+""".resident_date='"""+id+"""' and """+tblas+""".employee_id=res.employee_id          
            
            """
      
    query="""
        select """+field+"""
            
        """+fromtbl+joining+where+groupBy+"""

        order by b.building_name, r.dormitory_room_name ASC
    """ 
    print(query)
    content = frappe.db.sql( query=query, values=values ,as_dict=False)


    data['content'] = content

    foot = []
    idxcount=8
    for i,vals in enumerate(content):
        for index,val in enumerate(vals):
            if index>=idxcount:
                if i==0:
                    foot.append(val)
                else:
                    isi = foot[index-idxcount]
                    isi +=val
                    foot[index-idxcount] = isi

    data['footer'] = foot

    return data