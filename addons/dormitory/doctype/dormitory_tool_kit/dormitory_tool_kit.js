// Copyright (c) 2023, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dormitory Tool Kit', {
	// refresh: function(frm) {

	// }
	onload: function (frm) {
		frm.set_query("dormitory_room_id", function() {
			return {
				"filters": {
					"dormitory_building_id": frm.doc.dormitory_building_id,
				}
			};
		});
	},
});
