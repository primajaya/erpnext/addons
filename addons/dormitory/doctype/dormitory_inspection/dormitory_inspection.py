# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class DormitoryInspection(Document):
	pass

@frappe.whitelist()
def parameter_query(category):
	
	tabParameterCategory = frappe.qb.DocType("Dormitory Parameter Category")
	tabParameter = frappe.qb.DocType("Dormitory Parameter")
	tabParameterItem = frappe.qb.DocType("Dormitory Parameter Item")

	return (
          frappe.qb.from_(tabParameterItem).as_("tpi")
		  .left_join(tabParameter).on(tabParameter.name == tabParameterItem.parent)
		  .left_join(tabParameterCategory).on(tabParameterCategory.name == tabParameter.dormitory_parameter_category_id)
		  .select(
			  tabParameterItem.name,
			  tabParameterItem.parameter_item_name,
			  tabParameterItem.parameter_item_point,
			  tabParameterItem.parent,
			  tabParameter.dormitory_parameter_name,

			  )
		  .where(tabParameter.dormitory_parameter_category_id == category)
          ).run(as_dict=True)
	
	 
