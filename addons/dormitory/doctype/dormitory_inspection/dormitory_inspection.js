// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dormitory Inspection', {
	refresh: function(frm) {
		// frm.fields_dict.inspection_paramter.grid.grid_buttons.addClass('hidden');
		// frm.fields_dict.inspection_paramter.grid.wrapper.find('.btn-open-row').hide();

		frm.set_df_property('inspection_paramter', 'cannot_delete_rows', 1);
		frm.set_df_property('inspection_paramter', 'cannot_add_rows', 1);

		if(frm.doc.dormitory_parameter_cat_id && frm.is_new()){
			frm.trigger('inspection_paramter_item');
		}

		if(frm.is_new()){
			frm.set_value('dormitory_inspection_date', moment().toDate());
		}

		frm.set_query('dormitory_room_id', function() {
			if(frm.doc.dormitory_building_id){
				return {
					filters: {
						'dormitory_building_id': frm.doc.dormitory_building_id,
					}
				};
			}else{
				return None;
			}
		});
	},
	onload: function(frm){
		
	},
	dormitory_building_id: function(frm){
		frm.set_value('dormitory_room_id','')
	},

	dormitory_room_id: function(frm){
		if(! frm.doc.dormitory_building_id){
			frappe.msgprint(
				__(
					"Please select first {0}",
					[frm.fields_dict.dormitory_building_id._label]
				)
			);
		}
	},
	dormitory_parameter_cat_id: function(frm){
		if(!frm.doc.dormitory_inspection_date || !frm.doc.dormitory_building_id){
			if(frm.doc.dormitory_parameter_cat_id){
				frappe.msgprint(
					__(
						"Please select first {0} and {1}",
						[frm.fields_dict.dormitory_inspection_date._label, frm.fields_dict.dormitory_building_id._label]
					)
				);
			}
			frm.set_value('dormitory_parameter_cat_id','')
		}else{
			
			frm.trigger('inspection_paramter_item');
		}
	},

	inspection_paramter_item: function(frm){
		frm.set_value('inspection_paramter', []);
		// frm.refresh_field('inspection_paramter');
		frappe.call({
			method: "addons.dormitory.doctype.dormitory_inspection.dormitory_inspection.parameter_query",
			args: {category: frm.doc.dormitory_parameter_cat_id}
		}).done(records => {
			if(records.message){
				frm.doc.inspection_paramter = [];
				$.each(records.message, function(i, r) {
					frm.add_child('inspection_paramter', {
						dormitory_parameter_item_id: r.name,
						dormitory_parameter_item_name: r.parameter_item_name,
						dormitory_parameter_item_point: r.parameter_item_point,
						dormitory_parameter_id: r.parent,
						dormitory_parameter_name: r.dormitory_parameter_name,
						dormitory_parameter_cat_id: frm.doc.dormitory_parameter_cat_id,
						dormitory_building_id: frm.doc.dormitory_building_id,
						dormitory_room_id: frm.doc.dormitory_room_id,
						dormitory_inspection_date: frm.doc.dormitory_inspection_date,
					});
						
				});
				
				refresh_field('inspection_paramter');
				// frm.fields_dict.inspection_paramter.grid.wrapper.find('.btn-open-row').hide();
			}
		});
	}
});
