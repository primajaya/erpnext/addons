# Copyright (c) 2023, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import (
	add_days,
	cint,
	cstr,
	format_date,
	get_datetime,
	get_link_to_form,
	getdate,
	nowdate,
)
from erpnext.controllers.queries import (
	get_fields,
	get_filters_cond,
	get_match_cond,
)
from datetime import date, datetime, timedelta
import json
from frappe.desk.form.save import (
	capture_doc,
	set_local_name,
	run_onload,
	send_updated_docs,
	add_data_to_monitor,
)
from frappe.model.docstatus import DocStatus
from copy import copy
from addons.repository.dormitory_room_repo import (
	get_count_filled_room
	)
from addons.utils.logger import get_logger


log = get_logger(__name__)

class DormitoryResident(Document):
	pass

@frappe.whitelist()
def get_category_staff(building_id, employee_id):
	data = {
		'employee_cat':'Staff',
		'building_cat':'Staff'
		}
	
	tabDormitoryBuilding = frappe.qb.DocType("Dormitory Building")
	tabEmployee = frappe.qb.DocType("Employee")
	tabDesignation = frappe.qb.DocType("Designation")

	building_cat = frappe.db.get_value('Dormitory Building',{'name':building_id},['building_category'])
	data['building_cat'] = building_cat

	employee_cat = (
          frappe.qb.from_(tabEmployee).as_("e")
		  .left_join(tabDesignation).on(tabDesignation.name == tabEmployee.designation)
		  .select(tabDesignation.category)
		  .where(tabEmployee.name == employee_id)
          ).run(as_dict=False)
	
	data['employee_cat'] = employee_cat

	return data


@frappe.whitelist()
def mark_bulk_resident(data):
	import json

	if isinstance(data, str):
		data = json.loads(data)
	data = frappe._dict(data)
	# if not data.unmarked_days:
	# 	frappe.throw(_("Please select a date."))
	# 	return
	
	from_date = data.from_date
	to_date = data.to_date

	# room is full
	if data.type_log != "OUT":
		room_id = data.dormitory_room_id if data.type_log != "TRANSFER" else data.custom_dormitory_room_transfer
		count_filled_room = get_count_filled_room(room_id, data.resident_date, data.resident_date)

		room = frappe.db.get_value("Dormitory Room",filters={'name':room_id},fieldname=['name','dormitory_room_capacity'],as_dict=True)
		log.info("count_filled_room")
		log.info(count_filled_room)
  
		if room.dormitory_room_capacity == 0 or room.dormitory_room_capacity is None:
			frappe.throw(_("This room not set capacity"))
   
		if len(count_filled_room) > 0:
			count_available = room.dormitory_room_capacity-count_filled_room[0].count_filled
			if count_available<1:
				frappe.throw(_("This room is full!"))

	existing = frappe.db.get_value("Dormitory Resident", filters={
		"employee_id": data.employee_id,
		"type_log": "IN" if data.type_log=="TRANSFER" else data.type_log,
		# "resident_date": ['>=',from_date],
		# "resident_date": ['<=',to_date],
		"resident_date": ["between", (from_date, to_date)],
	})

	if existing and data.type_log != "TRANSFER":
		frappe.throw(_("Resident employee {0} between date {1} to {2} is already exist").format(data.employee_id, from_date, to_date))

	if data.type_log == "OUT":
		log.info("OUT")
		existing_IN = frappe.db.get_values("Dormitory Resident",filters={
			"employee_id": data.employee_id,
			"type_log": "IN",
			"dormitory_building_id":data.dormitory_building_id,
			"dormitory_room_id":data.dormitory_room_id,
			# "resident_date": ['>=',datetime.strptime(from_date,'%Y-%m-%d').date()],
			# "resident_date": ['<=',datetime.strptime(to_date,'%Y-%m-%d').date()]
			"resident_date": ["between", (from_date, to_date)],
		},as_dict=True)
		log.info(existing_IN)
		log.info(len(existing_IN))
		
		lendays = ((datetime.strptime(to_date,'%Y-%m-%d').date()-datetime.strptime(from_date,'%Y-%m-%d').date()).days+1)
		log.info(lendays)
		if len(existing_IN) != lendays: # or data.dormitory_building_id!=existing_IN.dormitory_building_id or data.dormitory_room_id!=existing_IN.dormitory_room_id:
			
			log.info("existing_IN")
			frappe.throw(_("This employee {0} has not been assigned type OUT to any building and room, set the employee to assign IN first!").format(data.employee_id, data.resident_date))
		
	
	
	if data.type_log == "TRANSFER":
		existing_emp_in_room = frappe.db.get_value("Dormitory Resident",filters={
			"dormitory_building_id": data.dormitory_building_id_transfer,
			"dormitory_room_id": data.dormitory_room_id_transfer,
			"employee_id": data.employee_id,
			"type_log": "IN",
			# "resident_date": ['>=',from_date],
			# "resident_date": ['<=',to_date],
			"resident_date": ["between", (from_date, to_date)],
		},as_dict=True)

		existing_emp_out_room = frappe.db.get_value("Dormitory Resident",filters={
			"dormitory_building_id": data.dormitory_building_id,
			"dormitory_room_id": data.dormitory_room_id,
			"employee_id": data.employee_id,
			"type_log": "OUT",
			# "resident_date": ['>=',from_date],
			# "resident_date": ['<=',to_date],
			"resident_date": ["between", (from_date, to_date)],
		},as_dict=True)

		if not existing_emp_in_room :
			frappe.throw(_("The employee {0} must assign IN first to be able to assign TRANSFER between date {1} to {2}").format(data.employee_id, from_date, to_date))
		
		if existing_emp_out_room :
			frappe.throw(_("Cant transfer employee {0}, resident type 'OUT' on between date {1} to {2} is already exist").format(data.employee_id, from_date, to_date))


	# for type_log IN
	while from_date <= to_date:
		log.info("while from_date")
		log.info(from_date)
		doc_dict = {
			"doctype": "Dormitory Resident",
			"dormitory_building_id": data.dormitory_building_id,
			"dormitory_room_id": data.dormitory_room_id,
			"employee_id": data.employee_id,
			"type_log": data.type_log,
			"resident_date": getdate(from_date),
			"description": "",
			"name":None
		}
		doc_dict['description'] = data.description

		log.info(doc_dict)

		old_data = frappe.db.get_value("Dormitory Resident",filters={
				"dormitory_building_id": data.dormitory_building_id,
				"dormitory_room_id": data.dormitory_room_id,
				"employee_id": data.employee_id,
				"resident_date": from_date #getdate(from_date)
			},fieldname=['name','type_log'],as_dict=True)
		
		log.info(old_data)
		
		if data.type_log == "TRANSFER":
			if old_data :
				doc_dict['type_log'] = "OUT"
				# doc_dict['description'] = " TRANSFER"
				resident_old = frappe.get_doc(doc_dict).insert()
				# resident_old.submit()
		
			doc_dict['type_log'] = "IN"
			# doc_dict['description'] += " TRANSFER"
			doc_dict['dormitory_building_id'] = data.dormitory_building_id_transfer
			doc_dict['dormitory_room_id'] = data.dormitory_room_id_transfer
			frappe.get_doc(doc_dict).insert()

		if not old_data or old_data.type_log != data.type_log:
			log.info("save no old data")
			resident = frappe.get_doc(doc_dict).insert()
		# resident.submit()
		from_date = add_days(from_date, 1)
			
@frappe.whitelist()
def get_data_by_date_n_emp_n_type_log(tgl,emp_id,type_log):

	return frappe.db.get_value(doctype='Dormitory Resident',fieldname='*',filters={'resident_date':tgl, 'employee_id':emp_id, 'type_log':type_log},as_dict=True)

@frappe.whitelist()
def save_resident(doc, action):
	print(doc)
	# import json

	# if isinstance(data, str):
	# 	data = json.loads(data)
	# data = frappe._dict(data)

	# doc_dict = {
	# 	"doctype": "Dormitory Resident",
	# 	"dormitory_building_id": data.dormitory_building_id,
	# 	"dormitory_room_id": data.dormitory_room_id,
	# 	"employee_id": data.employee_id,
	# 	"type_log": data.type_log,
	# 	"resident_date": data.resident_date,
	# 	"description": data.description,
	# 	"name":data.name
	# }
	# doc_dict_search = {
	# 	"doctype": "Dormitory Resident",
	# 	"dormitory_building_id": data.dormitory_building_id,
	# 	"dormitory_room_id": data.dormitory_room_id,
	# 	"employee_id": data.employee_id,
	# 	"type_log": data.type_log,
	# 	"resident_date": data.resident_date,
	# }

	# old_data = frappe.get_doc({"doctype": "Dormitory Resident","name": data.name}) #frappe.db.get_value("Dormitory Resident",{'name':data.name})
	# if not old_data:
	# 	old_data = frappe.get_doc(doc_dict_search)

	# if old_data:
	# 	if data.type_log == "TRANSFER":
	# 		doc_dict['type_log'] = "OUT"
	# 		doc_dict['description'] = data.description+" TRANSFER"
	# 		frappe.get_doc(doc_dict).insert()

	# 		doc_dict['type_log'] = "IN"
	# 		doc_dict['description'] = data.description+" TRANSFER"
	# 		doc_dict['dormitory_building_id'] = data.dormitory_building_id_transfer
	# 		doc_dict['dormitory_room_id'] = data.dormitory_room_id_transfer
	# 		resident =frappe.get_doc(doc_dict).insert()
	# 	else:
	# 		# doc_dict['name'] = data.name
	# 		# resident_update =frappe.get_doc({"doctype": "Dormitory Resident","name": data.name})
	# 		old_data.dormitory_building_id = data.dormitory_building_id
	# 		old_data.dormitory_room_id = data.dormitory_room_id
	# 		old_data.employee_id = data.employee_id
	# 		old_data.type_log = data.type_log
	# 		old_data.resident_date = data.resident_date
	# 		old_data.description = data.description
	# 		old_data.save()
	# else:
	# 	resident =frappe.get_doc(doc_dict).insert()

	"""save / submit / update doclist"""
	doc = frappe.get_doc(json.loads(doc))

	print(doc)
	name_new = doc.name
	data = copy(doc)

	# room is full
	if data.type_log != "OUT":
		room_id = data.dormitory_room_id if data.type_log != "TRANSFER" else data.custom_dormitory_room_transfer
		count_filled_room = get_count_filled_room(room_id, data.resident_date, data.resident_date)

		room = frappe.db.get_value("Dormitory Room",filters={'name':room_id},fieldname=['name','dormitory_room_capacity'],as_dict=True)
		# log.info("count_filled_room")
		# log.info(count_filled_room)
		# log.info(len(count_filled_room))
		# log.info(room)
		# log.info(room.dormitory_room_capacity)
		if room.dormitory_room_capacity == 0 or room.dormitory_room_capacity is None:
			frappe.throw(_("This room not set capacity"))
   
		if len(count_filled_room) > 0:
			count_available = room.dormitory_room_capacity-count_filled_room[0].count_filled
			if count_available<1:
				frappe.throw(_("This room is full!"))

	# get existing data
	existing = frappe.db.get_value(data.doctype,{'name': data.name},as_dict=True)
	if not existing:
		existing = frappe.db.get_value(data.doctype,{
			"dormitory_building_id": data.dormitory_building_id,
			"dormitory_room_id": data.dormitory_room_id,
			"employee_id": data.employee_id,
			"type_log": "IN" if data.type_log=="TRANSFER" else data.type_log,
			"resident_date": data.resident_date,
		},as_dict=True)

	existing_date = frappe.db.get_value(data.doctype,{
			"employee_id": data.employee_id,
			"type_log": "IN" if data.type_log=="TRANSFER" else data.type_log,
			"resident_date": data.resident_date,
		},as_dict=True)
	
	if data.type_log == "OUT":
		log.info("OUT")
		existing_IN = frappe.db.exists(data.doctype,{
			"employee_id": data.employee_id,
			"type_log": "IN",
			"resident_date": data.resident_date,
			"dormitory_building_id":data.dormitory_building_id,
			"dormitory_room_id":data.dormitory_room_id,
		})#,as_dict=True)
		log.info(existing_IN)
		if not existing_IN : # or data.dormitory_building_id!=existing_IN.dormitory_building_id or data.dormitory_room_id!=existing_IN.dormitory_room_id:
			
			log.info("existing_IN")
			frappe.throw(_("This employee {0} has not been assigned type OUT to any building and room, set the employee to assign IN first!").format(data.employee_id, data.resident_date))
		elif  existing_IN == data.name :	
			frappe.throw(_("Resident employee {0} can't Edit to type OUT, no data type IN in date {1} and this building or room if your edit").format(data.employee_id, data.resident_date))

	# if existing_date:
	# 	frappe.throw(_("Resident employee {0} in date {1} is already exist").format(data.employee_id, data.resident_date))
	

	if doc.get("__islocal") and doc.name.startswith("new-" + doc.doctype.lower().replace(" ", "-")) and doc.type_log !="TRANSFER" and existing_date:
		frappe.throw(_("Resident employee {0} in date {1} and type log {2} is already").format(data.employee_id, data.resident_date, data.type_log))
		
	
	if doc.get("__islocal") and existing :
		# doc.delete_key(doc,'__islocal")
		doc.set("__islocal",0)
		if doc.get("name") and doc.get("name") != existing.name :
			doc.set("name", existing.name)

	capture_doc(doc, action)
	if doc.get("__islocal") and doc.name.startswith("new-" + doc.doctype.lower().replace(" ", "-")):
		# required to relink missing attachments if they exist.
		doc.__temporary_name = doc.name
	set_local_name(doc)

	if data.type_log != "TRANSFER":
		# action
		doc.docstatus = {
			"Save": DocStatus.draft(),
			"Submit": DocStatus.submitted(),
			"Update": DocStatus.submitted(),
			"Cancel": DocStatus.cancelled(),
		}[action]

		doc.save()

		# # update recent documents
		# run_onload(doc)
		# send_updated_docs(doc)

		# add_data_to_monitor(doctype=doc.doctype, action=action)

		# frappe.msgprint(frappe._("Saved"), indicator="green", alert=True)

	else:
		if not existing:
			frappe.throw(_("Cant transfer employee {0}, no data resident type 'IN' on date {1}").format(data.employee_id, data.resident_date))

		existing_emp_in_room = frappe.db.get_value(doc.doctype,{
			"dormitory_building_id": data.custom_dormitory_building_transfer,
			"dormitory_room_id": data.custom_dormitory_room_transfer,
			"employee_id": data.employee_id,
			"type_log": "IN",
			"resident_date": data.resident_date,
		},as_dict=True)

		existing_emp_out_room = frappe.db.get_value(doc.doctype,{
			"dormitory_building_id": data.dormitory_building_id,
			"dormitory_room_id": data.dormitory_room_id,
			"employee_id": data.employee_id,
			"type_log": "OUT",
			"resident_date": data.resident_date,
		},as_dict=True)

		if existing_emp_in_room :
			frappe.throw(_("Cant transfer employee {0}, resident type 'IN' on date {1} already exist").format(data.employee_id, data.resident_date))
		
		if existing_emp_out_room :
			frappe.throw(_("Cant transfer employee {0}, resident type 'OUT' on date {1} already exist").format(data.employee_id, data.resident_date))


		doc_out = copy(data)
		doc_in = copy(data)
		capture_doc(doc_out, action)
		capture_doc(doc, action)
		if doc_out.get("__islocal") and doc_out.name.startswith("new-" + doc_out.doctype.lower().replace(" ", "-")):
			# required to relink missing attachments if they exist.
			doc.__temporary_name = doc_out.name
		set_local_name(doc_out)
		print(doc_out)
		doc_out.set('type_log', "OUT")
		doc_out.set('description', data.description+" TRANSFER")
		# doc_out.set('name',None)
		doc_out.save()
		# save_doc_out = frappe.get_doc(doc_out).insert()
		print(doc_out)

		# add_data_to_monitor(doctype=doc_out.doctype, action=action)

		print(doc_in)
		
		doc_in.set('type_log',"IN") 
		doc_in.set('description', data.description+" TRANSFER")
		doc_in.set('dormitory_building_id', data.custom_dormitory_building_transfer)
		doc_in.set('dormitory_room_id', data.custom_dormitory_room_transfer)
		capture_doc(doc, action)
		if doc_in.get("__islocal") and doc_in.name.startswith("new-" + doc_in.doctype.lower().replace(" ", "-")):
			# required to relink missing attachments if they exist.
			doc_in.__temporary_name = doc_in.name
		set_local_name(doc_in)
		# doc_in.set('name',None)
		print(doc_in)
		
		# doc =frappe.get_doc(doc_in).insert()
		doc =doc_in.save()
		# doc['localname']=name_new
		
		# doc.save()

	run_onload(doc)
	send_updated_docs(doc)

	add_data_to_monitor(doctype=doc.doctype, action=action)

	frappe.msgprint(frappe._("Saved"), indicator="green", alert=True)
		

	# result = {}
	# result['docs'] = [resident]
	# result['docinfo'] = {
	# 	"doctype": "Dormitory Resident",
    #     "name": resident.name
	# }
	# result['_server_messages']= [{"message": "Saved", "title": "Message", "indicator": "green", "alert": 1}]

	# return result
		
@frappe.whitelist()
def get_employee(emp_id):

	return frappe.db.get_value(doctype='Employee',fieldname='*',filters={'name':emp_id},as_dict=True)


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def room_query(doctype, txt, searchfield, start, page_len, filters):
	start_date = date.today()
	end_date = date.today()
	if isinstance(filters, str):
		filters = json.loads(filters)

	if filters:
		flt = filters
		if isinstance(filters, dict):
			filters = filters.items()
			flt = []
			for f in filters:
				if isinstance(f[1], str) and ( f[1] == "start_date" or f[1] == "end_date"):
					if f[1] == "start_date":
						start_date = f[3]
					else:
						end_date = f[3]
				else:
					flt.append(f)

			
		else:
			flt = []
			for f in filters:
				if isinstance(f[1], str) and ( f[1] == "start_date" or f[1] == "end_date"):
					if f[1] == "start_date":
						start_date = f[3]
					else:
						end_date = f[3]
				else:
					flt.append(f)

		filters = flt

	doctype = "Dormitory Room"
	conditions = []
	fields = get_fields(doctype, ["name", "dormitory_room_name"])
	days = (getdate(end_date)-getdate(start_date)).days+1
	print("=========")
	print(days)

	sql_str = """ select count(*) as count_filled , fill.dormitory_room_id, fill.resident_date
		from 
		(
			select 
			max(a.name) as name
			,max(a.type_log) as type_log
			,a.dormitory_room_id
			,resident_date
			from `tabDormitory Resident` as a
			group by employee_id, resident_date
			ORDER BY a.name DESC
		) as fill
		where fill.type_log ='IN' 
		group by fill.resident_date, fill.dormitory_room_id
		order by count(*) DESC """

	if days > 1 :
		sql_str = """
			select max(fill2.count_filled) as count_filled, fill2.dormitory_room_id, fill2.resident_date
			from ( """+ sql_str +""" ) as fill2 
			group by fill2.dormitory_room_id
			"""
	
	# print(sql_str)

	return frappe.db.sql(
		"""
		select 
			`tabDormitory Room`.name, 
			`tabDormitory Room`.dormitory_room_name,
			CONCAT(" Available ",(
				CASE 
					WHEN (`tabDormitory Room`.dormitory_room_capacity - filled.count_filled) < `tabDormitory Room`.dormitory_room_capacity  
						THEN (`tabDormitory Room`.dormitory_room_capacity-filled.count_filled) 
					ELSE `tabDormitory Room`.dormitory_room_capacity 
				END)
			) as available
		from `tabDormitory Room`
		left join ( """+ sql_str +""" ) as filled 
		on filled.dormitory_room_id = `tabDormitory Room`.name and filled.resident_date between %(start_date)s and %(end_date)s

		where ({key} like %(txt)s
				or `tabDormitory Room`.dormitory_room_name like %(txt)s)
			{fcond} {mcond}
		order by
			(case when locate(%(_txt)s, `tabDormitory Room`.name) > 0 then locate(%(_txt)s, `tabDormitory Room`.name) else 99999 end),
			(case when locate(%(_txt)s, `tabDormitory Room`.dormitory_room_name) > 0 then locate(%(_txt)s, `tabDormitory Room`.dormitory_room_name) else 99999 end),
			idx desc,
			`tabDormitory Room`.name, `tabDormitory Room`.dormitory_room_name
		limit %(page_len)s offset %(start)s""".format(
			**{
				# "fields": ", ".join(fields),
				"key": searchfield,
				"fcond": get_filters_cond(doctype, filters, conditions),
				"mcond": get_match_cond(doctype),
			}
		),
		{"txt": "%%%s%%" % txt, "_txt": txt.replace("%", ""), "start": start, "page_len": page_len, "start_date":start_date, "end_date":end_date},
	)
