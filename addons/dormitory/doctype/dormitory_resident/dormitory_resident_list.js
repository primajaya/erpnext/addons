frappe.listview_settings["Dormitory Resident"] = {
	add_fields: ["type_log", "resident_date"],

	
	// get_indicator: function(doc) {
	// 		var type_log_color = {
	// 			"IN": "green",
	// 			"OUT": "red"
	// 		};
	// 		return [__(doc.type_log), type_log_color[doc.type_log], "type log,=,"+doc.type_log];
	// },

	onload: function (list_view) {
		let me = this;

		list_view.page.add_inner_button(__("Mark Resident"), function () {
			let first_day_of_month = moment().startOf('month');

			if (moment().toDate().getDate() === 1) {
				first_day_of_month = first_day_of_month.subtract(1, "month");
			}

			let dialog = new frappe.ui.Dialog({
				title: __("Mark Resident"),
				fields: [
					{
						fieldname: "employee_id",
						label: __("Employee"),
						fieldtype: "Link",
						options: "Employee",
						get_query: () => {
							return {
								query: "erpnext.controllers.queries.employee_query",
							};
						},
						reqd: 1,
						onchange: () => me.get_employee_data(dialog),
					},
					{
						fieldname: "employee_name",
						label: __("Employee Name"),
						fieldtype: "Data",
						hidden:1
					},
					{
						fieldtype: "Section Break",
						fieldname: "time_period_section",
						// hidden: 1,
					},
					{
						label: __("Start Date"),
						fieldtype: "Date",
						fieldname: "from_date",
						reqd: 1,
						// default: first_day_of_month.toDate(),
						default: moment().toDate(),
						onchange: () => me.check_from_to_date(dialog),
					},
					{
						fieldtype: "Column Break",
						fieldname: "time_period_column",
					},
					{
						label: __("End Date"),
						fieldtype: "Date",
						fieldname: "to_date",
						reqd: 1,
						default: moment().toDate(),
						onchange: () => me.check_from_to_date(dialog),
					},
					{
						fieldtype: "Section Break",
						fieldname: "type_log_section",
					},
					{
						label: __("Type Log"),
						fieldtype: "Select",
						fieldname: "type_log",
						options: ["IN", "OUT", "TRANSFER"],
						default: "IN",
						reqd: 1,
						onchange: () => me.reset_dialog_transfer(dialog),
					},
					{
						fieldtype: "Section Break",
						fieldname: "building_n_room_section",
					},
					{
						fieldname: "dormitory_building_id",
						label: __("Dormitory Building"),
						fieldtype: "Link",
						options: "Dormitory Building",
						reqd: 1,
						onchange: function(){
							cur_dialog.fields_dict.dormitory_room_id.value=null;
							cur_dialog.set_df_property('dormitory_room_id','value',null);
							me.check_category_staff(dialog);
							
						},
					},
					{
						fieldname: "dormitory_room_id",
						label: __("Dormitory Room"),
						fieldtype: "Link",
						options: "Dormitory Room",
						get_query: () =>{
							if (cur_dialog.fields_dict.dormitory_building_id.value){
								return {
									query: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.room_query",
									"filters": [
										["Dormitory Room","dormitory_building_id","=", cur_dialog.fields_dict.dormitory_building_id.value],
										["Dormitory Room","name","!=", cur_dialog.fields_dict.custom_dormitory_room_transfer.value],
										["Dormitory Room","start_date",">=", cur_dialog.fields_dict.from_date.value],
										["Dormitory Room","end_date","<=", cur_dialog.fields_dict.to_date.value],
									]
								};
							}else{
								// frappe.msgprint(
								// 	__(
								// 		"Please Select First {0}",[cur_dialog.fields_dict.dormitory_building_id._label]
								// 	)
								// );
								frappe.show_alert({
									message: __(
										"Please Select First {0}",[cur_dialog.fields_dict.dormitory_building_id._label]
									),
									indicator: "red",
								});
								return None;
							}
						},
						
						reqd: 1,
						// onchange: () => me.reset_dialog(dialog),
					},
					{
						// fieldtype: "Section Break",
						// fieldname: "type_log_transfer_section",
						hidden: 0,
						fieldtype: "Column Break",
						fieldname: "type_log_transfer_section",
					},
					{
						fieldname: "custom_dormitory_building_transfer", //custom_dormitory_building_transfer ----d-ormitory_building_id_transfer
						label: __("Dormitory Building Transfer"),
						fieldtype: "Link",
						options: "Dormitory Building",
						hidden:1,
						// get_query: () => {
						// 	return {
						// 		query: "erpnext.controllers.queries.employee_query",
						// 	};
						// },
						reqd: 0,
						onchange: function() {
							dialog.fields_dict.custom_dormitory_room_transfer.value=null;
							dialog.set_df_property('custom_dormitory_room_transfer','value', null);
							me.reset_dormitory_room_id(dialog);
							// me.reset_dialog(dialog);
						},
					},
					{
						fieldname: "custom_dormitory_room_transfer", //custom_dormitory_room_transfer
						label: __("Dormitory Room Transfer"),
						fieldtype: "Link",
						options: "Dormitory Room",
						hidden:1,
						get_query: () =>{
							if (cur_dialog.fields_dict.custom_dormitory_building_transfer.value){
								return {
									query: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.room_query",
									"filters": [
										["Dormitory Room","dormitory_building_id","=", cur_dialog.fields_dict.custom_dormitory_building_transfer.value],
										["Dormitory Room","name","!=", cur_dialog.fields_dict.dormitory_room_id.value],
										["Dormitory Room","start_date",">=", cur_dialog.fields_dict.from_date.value],
										["Dormitory Room","end_date","<=", cur_dialog.fields_dict.to_date.value],
									]
								};
							}else{
								frappe.show_alert({
									message: __(
										"Please Select First {0}",[cur_dialog.fields_dict.custom_dormitory_building_transfer._label]
									),
									indicator: "red",
								});
								return None;
							}
						},
						
						reqd: 0,
						// onchange: () => me.reset_dialog(dialog),
					},

					// {
					// 	fieldtype: "Section Break",
					// 	fieldname: "days_section",
					// },
					// {
					// 	label: __("Exclude Holidays"),
					// 	fieldtype: "Check",
					// 	fieldname: "exclude_holidays",
					// 	onchange: () => me.get_unmarked_days(dialog),
					// },
					// {
					// 	label: __("Unmarked Attendance for days"),
					// 	fieldname: "unmarked_days",
					// 	fieldtype: "MultiCheck",
					// 	options: [],
					// 	columns: 2,
					// 	select_all: true,
					// },
					
					{
						fieldtype: "Section Break",
						fieldname: "description_section",
					},
					{
						label: __("Description"),
						fieldtype: "Text",
						fieldname: "description",
						reqd: 0,
					},
					
					
				],
				primary_action(data) {
					// if (cur_dialog.no_unmarked_days_left) {
					// 	frappe.msgprint(
					// 		__(
					// 			"Resident from {0} to {1} has already been marked for the Employee {2}",
					// 			[data.from_date, data.to_date, data.employee]
					// 		)
					// 	);
					// } else {
						frappe.confirm(
							__("Mark resident as {0} for {1} on selected dates?", [
								data.type_log,
								data.employee_id,
							]),
							() => {
								frappe.call({
									method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.mark_bulk_resident",
									args: {
										data: data,
									},
									callback: function (r) {
										if (r.message === 1) {
											frappe.show_alert({
												message: __("Resident Marked"),
												indicator: "blue",
											});
											cur_dialog.hide();
										}
									},
								});
							}
						);
					// }
					dialog.hide();
					list_view.refresh();
				},
				primary_action_label: __("Mark Resident"),
			});
			dialog.show();
		});

		
	},
	reset_dormitory_room_id: function(dialog){
		let fields = dialog.fields_dict;
		fields.dormitory_room_id.value="";
	},

	get_employee_data: function(dialog){
		let fields = dialog.fields_dict;
		if(fields.employee_id.value){
			frappe.call({
				method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.get_employee",
				async: false,
				args: {
					emp_id: fields.employee_id.value,
				},
			})
			.then((r) => {
				if(r.message){
					fields.employee_name.value = r.message.employee_name
					dialog.set_df_property("employee_name", "hidden", 0);
				}
			});
		}
		this.check_category_staff(dialog);
	},

	reset_dialog_transfer: function(dialog){
		let fields = dialog.fields_dict;
		if(fields.type_log.value=="TRANSFER"){
			// dialog.set_df_property("type_log_transfer_section", "hidden", 0);
			// dialog.set_df_property({
			// 	custom_dormitory_building_transfer:{
			// 		 hidden: 0,
			// 		 reqd: 1
			// 	}
			// });
			dialog.set_df_property("custom_dormitory_building_transfer", "hidden", 0);
			dialog.set_df_property("custom_dormitory_room_transfer", "hidden", 0);

			dialog.set_df_property("custom_dormitory_building_transfer", "reqd", 1);
			dialog.set_df_property("custom_dormitory_room_transfer", "reqd", 1);
		}else{
			// dialog.set_df_property("type_log_transfer_section", "hidden", 1);
			// dialog.set_df_property({
			// 	custom_dormitory_building_transfer:{
			// 		 hidden: 1,
			// 		 reqd: 0
			// 	}
			// });
			dialog.set_df_property("custom_dormitory_building_transfer", "hidden", 1);
			dialog.set_df_property("custom_dormitory_room_transfer", "hidden", 1);

			dialog.set_df_property("custom_dormitory_building_transfer", "reqd", 0);
			dialog.set_df_property("custom_dormitory_room_transfer", "reqd", 0);
		}
	},

	check_from_to_date: function(dialog){
		let fields = dialog.fields_dict;
		this.set_min_to_date(dialog);
		// this.reset_dialog(dialog);
		if(fields.to_date.value < fields.from_date.value){
			frappe.throw(
				__(
					"This {0} must more then or quals {1}",
					[fields.to_date._label, fields.from_date._label]
				)
			);
			// frappe.show_alert({
			// 	message: __(
			// 		"This {0} must more then or quals {1}",
			// 		[fields.to_date._label, fields.from_date._label]
			// 	),
			// 	indicator: "red",
			// });
		}
	},

	set_min_to_date: function(dialog){
		let fields = dialog.fields_dict;
		fields.to_date.datepicker.update({
            minDate: moment(fields.from_date.value).toDate(),
			
        });
	},

	check_category_staff: function(dialog){
		let fields = dialog.fields_dict;
		let building_id = fields.dormitory_building_id.value;
		let employee_id = fields.employee_id.value;
		if (building_id && employee_id) {
			frappe.call({
				method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.get_category_staff",
				async: false,
				args: {
					employee_id: employee_id,
					building_id: building_id
				},
			})
			.then((r) => {
				if(r.message.employee_cat!=r.message.building_cat){
					frappe.throw(__(
						"This employee {0} assign to building {1}",
						[r.message.employee_cat, r.message.building_cat]
					))
					// frappe.msgprint(
					// 	__(
					// 		"This employee {0} assign to building {1}",
					// 		[r.message.employee_cat, r.message.building_cat]
					// 	)
					// );
					// frappe.show_alert({
					// 	message: __(
					// 		"This employee {0} assign to building {1}",
					// 		[r.message.employee_cat, r.message.building_cat]
					// 	),
					// 	indicator: "yellow",
					// });
				}
			});
				
		}
	},

	reset_dialog: function (dialog) { 
		let fields = dialog.fields_dict;

		// dialog.set_df_property("days_section", "hidden", 1);
		// dialog.set_df_property("unmarked_days", "options", []);
		// dialog.no_unmarked_days_left = false;
		// fields.exclude_holidays.value = false;

		// this.get_unmarked_days(dialog)
	},

	get_unmarked_days: function (dialog) {
		let fields = dialog.fields_dict;
		if (fields.employee_id.value && fields.from_date.value && fields.to_date.value) {
			// dialog.set_df_property("days_section", "hidden", 0);
			// dialog.set_df_property("status", "hidden", 0);
			dialog.set_df_property("exclude_holidays", "hidden", 0);
			dialog.no_unmarked_days_left = false;

			frappe
				.call({
					method: "hrms.hr.doctype.attendance.attendance.get_unmarked_days",
					async: false,
					args: {
						employee: fields.employee_id.value,
						from_date: fields.from_date.value,
						to_date: fields.to_date.value,
						exclude_holidays: fields.exclude_holidays.value,
					},
				})
				.then((r) => {
					var options = [];

					for (var d in r.message) {
						var momentObj = moment(r.message[d], "YYYY-MM-DD");
						var date = momentObj.format("DD-MM-YYYY");
						options.push({
							label: date,
							value: r.message[d],
							checked: 1,
						});
					}

					dialog.set_df_property(
						"unmarked_days",
						"options",
						options.length > 0 ? options : []
					);
					dialog.no_unmarked_days_left = options.length === 0;
				});
		}
	},
};
