// Copyright (c) 2023, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dormitory Resident', {
	refresh: function(frm) {
		frm.set_df_property("employee_id", "read_only", frm.is_new() ? 0 : 1);
		frm.set_df_property("resident_date", "read_only", frm.is_new() ? 0 : 1);

		// frm.add_custom_button(__('Save'), function(){
		// 	console.log(frm);
		// 	frappe.call({
		// 		method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.save_resident",
		// 		args: {
		// 			doc: frm.doc,
		// 		},
		// 		callback: function (r) {
		// 			if (r.message === 1) {
		// 				frappe.show_alert({
		// 					message: __("Resident Marked"),
		// 					indicator: "blue",
		// 				});
		// 			}
		// 		},
		// 	});
		// }).addClass('btn btn-primary btn-sm primary-action');
	},
	onload: function (frm) {
		// frm.disable_save();

		frm.set_query("employee_id", function() {
			return {
				query: "erpnext.controllers.queries.employee_query",
			};
		});

		frm.set_query("dormitory_room_id", function() {
			if(frm.doc.dormitory_building_id){
				return {
					query:"addons.dormitory.doctype.dormitory_resident.dormitory_resident.room_query",
					"filters": [
						["Dormitory Room","dormitory_building_id","=", frm.doc.dormitory_building_id],
						["Dormitory Room","name","!=", frm.doc.custom_dormitory_room_transfer],
						["Dormitory Room","start_date",">=", frm.doc.resident_date],
						["Dormitory Room","end_date","<=", frm.doc.resident_date],
					]
				};
			}else{
				return None;
			}
		});

		frm.set_query("custom_dormitory_room_transfer", function() {
			if(frm.doc.custom_dormitory_building_transfer){
				return {
					query: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.room_query",
					"filters": [
						["Dormitory Room","dormitory_building_id","=", frm.doc.custom_dormitory_building_transfer],
						["Dormitory Room","name","!=", frm.doc.dormitory_room_id],
						["Dormitory Room","start_date",">=", frm.doc.resident_date],
						["Dormitory Room","end_date","<=", frm.doc.resident_date],
					]
				};
			}else{
				return None;
			}
		});
		frm.set_df_property("custom_type_log_transfer_column_break", "hidden", 1);
		frm.set_df_property("custom_dormitory_building_transfer", "hidden", 1);
		frm.set_df_property("custom_dormitory_room_transfer", "hidden", 1);
		
		frappe.ui.form.save = function(frm, action, callback, btn) {
			$(btn).prop("disabled", true);
			const working_label = {
			  Save: __("Saving", null, "Freeze message while saving a document"),
			  Submit: __("Submitting", null, "Freeze message while submitting a document"),
			  Update: __("Updating", null, "Freeze message while updating a document"),
			  Amend: __("Amending", null, "Freeze message while amending a document"),
			  Cancel: __("Cancelling", null, "Freeze message while cancelling a document")
			}[toTitle(action)];
			var freeze_message = working_label ? __(working_label) : "";
			var save = function() {
			  remove_empty_rows();
			  $(frm.wrapper).addClass("validated-form");
			  if ((action !== "Save" || frm.is_dirty()) && check_mandatory()) {
				_call({
				  method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.save_resident",//"frappe.desk.form.save.savedocs",
				  args: { doc: frm.doc, action },
				  callback: function(r) {
					$(document).trigger("save", [frm.doc]);
					callback(r);
				  },
				  error: function(r) {
					callback(r);
				  },
				  btn,
				  freeze_message
				});
			  } else {
				!frm.is_dirty() && frappe.show_alert({ message: __("No changes in document"), indicator: "orange" });
				$(btn).prop("disabled", false);
			  }
			};
			var remove_empty_rows = function() {
			  const docs = frappe.model.get_all_docs(frm.doc);
			  const tables = docs.filter((d) => {
				return frappe.model.is_table(d.doctype);
			  });
			  let modified_table_fields = [];
			  tables.map((doc) => {
				const cells = frappe.meta.docfield_list[doc.doctype] || [];
				const in_list_view_cells = cells.filter((df) => {
				  return cint(df.in_list_view) === 1;
				});
				const is_empty_row = function(cells2) {
				  for (let i = 0; i < cells2.length; i++) {
					if (locals[doc.doctype][doc.name] && locals[doc.doctype][doc.name][cells2[i].fieldname]) {
					  return false;
					}
				  }
				  return true;
				};
				if (is_empty_row(in_list_view_cells)) {
				  frappe.model.clear_doc(doc.doctype, doc.name);
				  modified_table_fields.push(doc.parentfield);
				}
			  });
			  modified_table_fields.forEach((field) => {
				frm.refresh_field(field);
			  });
			};
			var cancel = function() {
			  var args = {
				doctype: frm.doc.doctype,
				name: frm.doc.name
			  };
			  var workflow_state_fieldname = frappe.workflow.get_state_fieldname(frm.doctype);
			  if (workflow_state_fieldname) {
				$.extend(args, {
				  workflow_state_fieldname,
				  workflow_state: frm.doc[workflow_state_fieldname]
				});
			  }
			  _call({
				method: "frappe.desk.form.save.cancel",
				args,
				callback: function(r) {
				  $(document).trigger("save", [frm.doc]);
				  callback(r);
				},
				btn,
				freeze_message
			  });
			};
			var check_mandatory = function() {
			  var has_errors = false;
			  frm.scroll_set = false;
			  if (frm.doc.docstatus == 2)
				return true;
			  $.each(frappe.model.get_all_docs(frm.doc), function(i, doc) {
				var error_fields = [];
				var folded = false;
				$.each(frappe.meta.docfield_list[doc.doctype] || [], function(i2, docfield) {
				  if (docfield.fieldname) {
					const df = frappe.meta.get_docfield(doc.doctype, docfield.fieldname, doc.name);
					if (df.fieldtype === "Fold") {
					  folded = frm.layout.folded;
					}
					if (is_docfield_mandatory(doc, df) && !frappe.model.has_value(doc.doctype, doc.name, df.fieldname)) {
					  has_errors = true;
					  error_fields[error_fields.length] = __(df.label);
					  if (!frm.scroll_set) {
						scroll_to(doc.parentfield || df.fieldname);
					  }
					  if (folded) {
						frm.layout.unfold();
						folded = false;
					  }
					}
				  }
				});
				if (frm.is_new() && frm.meta.autoname === "Prompt" && !frm.doc.__newname) {
				  has_errors = true;
				  error_fields = [__("Name"), ...error_fields];
				}
				if (error_fields.length) {
				  let meta = frappe.get_meta(doc.doctype);
				  if (meta.istable) {
					const table_field = frappe.meta.docfield_map[doc.parenttype][doc.parentfield];
					const table_label = __(table_field.label || frappe.unscrub(table_field.fieldname)).bold();
					var message = __("Mandatory fields required in table {0}, Row {1}", [
					  table_label,
					  doc.idx
					]);
				  } else {
					var message = __("Mandatory fields required in {0}", [__(doc.doctype)]);
				  }
				  message = message + "<br><br><ul><li>" + error_fields.join("</li><li>") + "</ul>";
				  frappe.msgprint({
					message,
					indicator: "red",
					title: __("Missing Fields")
				  });
				  frm.refresh();
				}
			  });
			  return !has_errors;
			};
			let is_docfield_mandatory = function(doc, df) {
			  if (df.reqd)
				return true;
			  if (!df.mandatory_depends_on || !doc)
				return;
			  let out = null;
			  let expression = df.mandatory_depends_on;
			  let parent = frappe.get_meta(df.parent);
			  if (typeof expression === "boolean") {
				out = expression;
			  } else if (typeof expression === "function") {
				out = expression(doc);
			  } else if (expression.substr(0, 5) == "eval:") {
				try {
				  out = frappe.utils.eval(expression.substr(5), { doc, parent });
				  if (parent && parent.istable && expression.includes("is_submittable")) {
					out = true;
				  }
				} catch (e) {
				  frappe.throw(__('Invalid "mandatory_depends_on" expression'));
				}
			  } else {
				var value = doc[expression];
				if ($.isArray(value)) {
				  out = !!value.length;
				} else {
				  out = !!value;
				}
			  }
			  return out;
			};
			const scroll_to = (fieldname) => {
			  frm.scroll_to_field(fieldname);
			  frm.scroll_set = true;
			};
			var _call = function(opts) {
			  if (frappe.ui.form.is_saving) {
				console.log("Already saving. Please wait a few moments.");
				throw "saving";
			  }
			  if (frm.is_new()) {
				frappe.ui.form.remove_old_form_route();
			  }
			  frappe.ui.form.is_saving = true;
			  return frappe.call({
				freeze: true,
				method: opts.method,
				args: opts.args,
				btn: opts.btn,
				callback: function(r) {
				  opts.callback && opts.callback(r);
				},
				error: opts.error,
				always: function(r) {
				  $(btn).prop("disabled", false);
				  frappe.ui.form.is_saving = false;
				  if (r) {
					var doc = r.docs && r.docs[0];
					if (doc) {
					  frappe.ui.form.update_calling_link(doc);
					}
				  }
				}
			  });
			};
			if (action === "cancel") {
			  cancel();
			} else {
			  save();
			}
		  };
	
	
	},

	type_log: function (frm) {
		if (frm.doc.type_log == "TRANSFER"){
			frm.set_df_property("custom_type_log_transfer_column_break", "hidden", 0);
			frm.set_df_property("custom_dormitory_building_transfer", "hidden", 0);
			frm.set_df_property("custom_dormitory_room_transfer", "hidden", 0);
			frm.set_df_property("custom_dormitory_building_transfer", "reqd", 1);
			frm.set_df_property("custom_dormitory_room_transfer", "reqd", 1);
		}else{
			frm.set_df_property("custom_type_log_transfer_column_break", "hidden", 1);
			frm.set_df_property("custom_dormitory_building_transfer", "hidden", 1);
			frm.set_df_property("custom_dormitory_room_transfer", "hidden", 1);
			frm.set_df_property("custom_dormitory_building_transfer", "reqd", 0);
			frm.set_df_property("custom_dormitory_room_transfer", "reqd", 0);
		}
		frm.trigger("set_building_n_room");
	},

	employee_id: function(frm) {
		frm.trigger("set_building_n_room");
	},

	resident_date: function (frm){
		frm.trigger("set_building_n_room");
	},

	set_building_n_room: function (frm){
		if (frm.doc.resident_date && frm.doc.employee_id && frm.doc.type_log){
			frappe.call({
				method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.get_data_by_date_n_emp_n_type_log",
				args: {
					tgl: frm.doc.resident_date,
					emp_id: frm.doc.employee_id,
					type_log: frm.doc.type_log =="TRANSFER" ?"IN":frm.doc.type_log
				},
				callback: function (r) {
					if (r.message) {
						console.log(r.message);
						if(frm.doc.type_log != "TRANSFER"){
							frappe.msgprint(
								__("Resident employee {0} in date {1} and type log {2} is already",[frm.doc.employee_id, frm.doc.resident_date, frm.doc.type_log])
							);
							// frappe.show_alert({
							// 	message: __("Resident employee {0} in date {1} and type log {2} is already",[frm.doc.employee_id, frm.doc.resident_date, frm.doc.type_log]),
							// 	indicator: "red",
							// });
						}
						frm.set_value({
							dormitory_building_id: r.message.dormitory_building_id,
							dormitory_room_id: r.message.dormitory_room_id,
							description: r.message.description
						});
					}else{
						// if(frm.doc.type_log != "TRANSFER"){
							frm.set_value({
								dormitory_building_id: '',
								dormitory_room_id: '',
								description:''
							});
						// }
					}
				},
			});
		}
	},
	dormitory_building_id: function(frm){
		frm.set_value({
			dormitory_room_id: ''
		});
		frm.trigger("check_category_staff");
	},
	employee_id:function(frm){
		frm.trigger("check_category_staff");
	},
	check_category_staff: function(frm){
		let fields = frm.fields_dict;
		let building_id = fields.dormitory_building_id.value;
		let employee_id = fields.employee_id.value;
		if (building_id && employee_id) {
			frappe.call({
				method: "addons.dormitory.doctype.dormitory_resident.dormitory_resident.get_category_staff",
				async: false,
				args: {
					employee_id: employee_id,
					building_id: building_id
				},
			})
			.then((r) => {
				if(r.message.employee_cat!=r.message.building_cat){
					frappe.throw(__(
						"This employee {0} assign to building {1}",
						[r.message.employee_cat, r.message.building_cat]
					))
					// frappe.msgprint(
					// 	__(
					// 		"This employee {0} assign to building {1}",
					// 		[r.message.employee_cat, r.message.building_cat]
					// 	)
					// );
					// frappe.show_alert({
					// 	message: __(
					// 		"This employee {0} assign to building {1}",
					// 		[r.message.employee_cat, r.message.building_cat]
					// 	),
					// 	indicator: "yellow",
					// });
				}
			});
				
		}
	},

	// on_submit: function(frm){
	// 	console.log("save");
	// }
	validate: function (frm) {
		
	}
});
