// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dormitory Inspection All', {
	refresh: function(frm) {
		frm.set_df_property('dormitory_insp_detail', 'cannot_delete_rows', 1);
		frm.set_df_property('dormitory_insp_detail', 'cannot_add_rows', 1);

		if(frm.is_new()) frm.set_value('dormitory_inspection_date', moment());
		frm.set_df_property('dormitorty_insp_parameter_detail_id','ignore_link_validation', 1);

		frm.set_query('dormitory_room_id', function() {
			if(frm.doc.dormitory_building_id){
				return {
					filters: {
						'dormitory_building_id': frm.doc.dormitory_building_id,
					}
				};
			}else{
				return None;
			}
		});
	},
	
	dormitory_insp_parameter_id: function(frm){
		frm.set_value('dormitory_building_id','');
		frm.set_value('dormitory_room_id','');
		frm.set_value('dormitorty_insp_parameter_detail_id','');
		frm.set_value('dormitorty_insp_parameter_detail_name','');

		frm.set_df_property('dormitory_building_id','hidden',1);
		frm.set_df_property('dormitory_building_id','reqd',0);
		frm.set_df_property('dormitory_room_id','hidden',1);
		frm.set_df_property('dormitory_room_id','reqd',0);
		frm.set_df_property('dormitorty_insp_parameter_detail_id','hidden',1);
		frm.set_df_property('dormitorty_insp_parameter_detail_id','reqd',0);
		frm.set_df_property('dormitorty_insp_parameter_detail_name','hidden',1);

		if(frm.doc.dormitory_insp_parameter_type == "Building"){
			frm.set_df_property('dormitory_building_id','hidden',0);
			frm.set_df_property('dormitory_building_id','reqd',1);
		} else 
		if(frm.doc.dormitory_insp_parameter_type == "Room"){
			frm.set_df_property('dormitory_building_id','hidden',0);
			frm.set_df_property('dormitory_building_id','reqd',1);
			frm.set_df_property('dormitory_room_id','hidden',0);
			frm.set_df_property('dormitory_room_id','reqd',1);
		} else 
		if(frm.doc.dormitory_insp_parameter_type == "Custom"){
			frm.set_df_property('dormitorty_insp_parameter_detail_id','hidden',0);
			frm.set_df_property('dormitorty_insp_parameter_detail_id','reqd',1);

			frm.set_df_property('dormitorty_insp_parameter_detail_name','hidden',0);

			frm.set_query('dormitorty_insp_parameter_detail_id', function() {
				if(frm.doc.dormitory_insp_parameter_id){
					return {
						query: 'addons.dormitory.doctype.dormitory_inspection_parameter_detail.dormitory_inspection_parameter_detail.get_query_link_combo',
						filters: {
							'parent': frm.doc.dormitory_insp_parameter_id,
						},
						
					};
				}else{
					return None;
				}
			});
		}

		if(frm.doc.dormitory_insp_parameter_type == "Custom"){
			frappe.call({
				method: 'frappe.client.get_list',
				args: {
					doctype: 'Dormitory Inspection Parameter Custom Header',/// Detail',
					parent: 'Dormitory Inspection Parameter',
					filters: {parent: frm.doc.dormitory_insp_parameter_id},
					fields: ['name','dor_insp_param_custom_header_name'],
					as_dict: true,
					limit_page_length: 100
				}	
			}).then(records => {
				console.log(records.message);
				frm.doc.dormitory_insp_detail = [];
				if(records.message){
					$.each(records.message, function(i, r) {
						frm.add_child('dormitory_insp_detail', {
							dormitory_insp_detail_header_id: r.name,
							dormitory_insp_detail_header_name: r.dor_insp_param_custom_header_name,
							dormitory_insp_date:frm.doc.dormitory_inspection_date,
						});
					});
					frm.refresh_field('dormitory_insp_detail');

					// frm.toggle_display([
					// 	'she_insp_detail_header',
					// ], 1);
					
					// frm.fields_dict.she_insp_detail.grid.update_docfield_property(
					// 	'she_insp_detail_header',
					// 	'in_list_view',
					// 	1
					// );
					// frm.fields_dict.she_insp_detail.grid.update_docfield_property(
					// 	'she_insp_detail_sub_param_2',
					// 	'in_list_view',
					// 	0
					// );
					// console.log('frm.fields_dict.she_insp_detail.grid.grid_rows');
					// console.log(frm.fields_dict.she_insp_detail.grid.grid_rows);
					for (let row of frm.fields_dict.dormitory_insp_detail.grid.grid_rows) {
						let docfield = row.docfields.find((d) => d.fieldname === 'dormitory_insp_detail_header_name');
						console.log('docfield');
						console.log(docfield);
						if (docfield) {
							docfield.in_list_view = 1;
						} 
						// else {
						// 	throw `field she_insp_detail_header not found`;
						// }
					}
				
					let field = frm.fields_dict.dormitory_insp_detail.grid.user_defined_columns.find((d) => d.fieldname === 'dormitory_insp_detail_header_name');
					console.log('field');
					console.log(frm.fields_dict.dormitory_insp_detail.grid);
					if(field){
						field.in_list_view = 1;
					}
					frm.refresh_field('dormitory_insp_detail_header_name');
				}
				
				frm.refresh_field('dormitory_insp_detail');
				// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();
			});
		}else {
			frappe.call({
				method: 'frappe.client.get_list',
				args: {
					doctype: 'Dormitory Inspection Parameter Detail',
					parent: 'Dormitory Inspection Parameter',
					filters: {parent: frm.doc.dormitory_insp_parameter_id},
					fields: ['name', 'dor_insp_det_param', 'dor_insp_det_sub_param', 'dor_insp_det_point'],
					as_dict: true,
					limit_page_length: 100
				}	
			}).then(records => {

				frm.doc.dormitory_insp_detail = [];
				if(records.message){
					$.each(records.message, function(i, r) {
						frm.add_child('dormitory_insp_detail', {
							dormitory_insp_parameter_detail_id: r.name,
							dormitory_insp_detail_parameter: r.dor_insp_det_param,
							dormitory_insp_detail_sub_parameter:r.dor_insp_det_sub_param,
							dormitory_insp_detail_point:r.dor_insp_det_pont,
							dormitory_insp_date:frm.doc.dormitory_inspection_date,
						});
					});
				}

				frm.refresh_field('dormitory_insp_detail');
				// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();
			});
		}
	},
	dormitorty_insp_parameter_detail_id: function(frm){
		if(frm.doc.dormitory_insp_parameter_type == "Custom"){
			frappe.call({
				method: "frappe.client.get_value",
				args:{
					doctype:'Dormitory Inspection Parameter Detail',
					fieldname: ['name', 'dor_insp_det_param', 'dor_insp_det_sub_param', 'dor_insp_det_point'],
					filters:{name:frm.doc.dormitorty_insp_parameter_detail_id},
					parent: 'Dormitory Inspection Parameter'
				},
				callback: function(r){
					if(r.message){
						let val = r.message;
						frm.set_value('dormitorty_insp_parameter_detail_name',val.dor_insp_det_param);

						/// inject item to child table

						for (let row of frm.fields_dict.dormitory_insp_detail.grid.grid_rows) {
							// console.log(row);
							// console.log(row.doc.doctype);
							// console.log(row.doc.name);
							frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_parameter_detail_id',val.name);
							frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_detail_parameter',val.dor_insp_det_param);
							frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_detail_sub_parameter',val.dor_insp_det_sub_param);
							frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_detail_point',val.dor_insp_det_point);
							frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_date',frm.doc.dormitory_inspection_date,);
							
						}
					}
					frm.refresh_field('dormitory_insp_detail');
				}
			});
		}
	},
	dormitory_building_id: function(frm){
		if(frm.doc.dormitory_insp_parameter_type != "Custom"){
			for (let row of frm.fields_dict.dormitory_insp_detail.grid.grid_rows) {
				frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_building_id',frm.doc.dormitory_building_id);
			}
		}
	},
	dormitory_room_id: function(frm){
		if(frm.doc.dormitory_insp_parameter_type != "Custom"){
			for (let row of frm.fields_dict.dormitory_insp_detail.grid.grid_rows) {
				frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_room_id',frm.doc.dormitory_room_id);
			}
		}
	},
	dormitory_inspection_date: function(frm){
		for (let row of frm.fields_dict.dormitory_insp_detail.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'dormitory_insp_date',frm.doc.dormitory_inspection_date);
			
		}
	}

});
