# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

from erpnext.controllers.queries import get_fields
import frappe
from frappe.desk.reportview import get_filters_cond, get_match_cond
from frappe.model.document import Document

class DormitoryInspectionParameterDetail(Document):
	pass


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_query_link_combo(doctype, txt, searchfield, start, page_len, filters):
	doctype = "Dormitory Inspection Parameter Detail"
	conditions = []
	fields = get_fields(doctype, ['name','dor_insp_det_param','dor_insp_det_sub_param'])

	return frappe.db.sql(
		"""select {fields} from `tabDormitory Inspection Parameter Detail`
		where {key} like %(txt)s
			 {fcond} {mcond}
		order by
			(case when locate(%(_txt)s, name) > 0 then locate(%(_txt)s, name) else 99999 end),
			idx desc,
			name, dor_insp_det_param, dor_insp_det_sub_param
		limit %(page_len)s offset %(start)s""".format(
			**{
				"fields": ", ".join(fields),
				"key": searchfield,
				"fcond": get_filters_cond(doctype, filters, conditions, True),
				"mcond": get_match_cond(doctype),
			}
		),
		{"txt": "%%%s%%" % txt, "_txt": txt.replace("%", ""), "start": start, "page_len": page_len}
	)