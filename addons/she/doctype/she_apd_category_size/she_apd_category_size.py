# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

from erpnext.controllers.queries import get_fields
import frappe
from frappe.desk.reportview import get_filters_cond, get_match_cond
from frappe.model.document import Document

from addons.utils.logger import get_logger


log = get_logger(__name__)

class SHEAPDCategorySize(Document):
	pass


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def apd_category_size_query(doctype, txt, searchfield, start, page_len, filters):
	doctype = "SHE APD Category Size"
	conditions = []
	fields = get_fields(doctype, ["name", "she_apd_category_size_number"])

	return frappe.db.sql(
		"""select {fields} from `tabSHE APD Category Size`
		where {key} like %(txt)s
			 {fcond} {mcond}
		order by
			(case when locate(%(_txt)s, name) > 0 then locate(%(_txt)s, name) else 99999 end),
			idx desc,
			name, she_apd_category_size_number
		limit %(page_len)s offset %(start)s""".format(
			**{
				"fields": ", ".join(fields),
				"key": searchfield,
				"fcond": get_filters_cond(doctype, filters, conditions, True),
				"mcond": get_match_cond(doctype),
			}
		),
		{"txt": "%%%s%%" % txt, "_txt": txt.replace("%", ""), "start": start, "page_len": page_len}
	)
 
@frappe.whitelist()
def get_data_she_apd_cat_size(name, parent):
	return frappe.db.get_value('SHE APD Category Size', 
                            filters={
								'name': name,
								'parent': parent
							},
                            fieldname=['name','she_apd_category_size_number'],
                            as_dict=True)
	