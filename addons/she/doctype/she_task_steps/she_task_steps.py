# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from addons.repository.she_task_steps import get_child_table_task_steps
from frappe.model.document import Document

class SHETaskSteps(Document):
	pass

@frappe.whitelist()
def get_data_for_child_table(designation):
    return get_child_table_task_steps(designation)