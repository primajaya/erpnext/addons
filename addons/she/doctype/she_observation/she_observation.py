# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname
from addons.utils.logger import get_logger


log = get_logger(__name__)

class SHEObservation(Document):
	pass


	def autoname(self):
     	#rev_number = {SITE_OBSERVER}-YYMMDD-{DEPT}-{NRP-OBSERVER}-#####
		self.rev_number = make_autoname("{0}-.YY.MM.DD.-{1}-{2}-.#####").format(self.branch_abbr, self.observation_section_abbr, self.observer_employee_id)
		# self.eqp_number = make_autoname("{0}-.YY.MM.DD.-{1}-.#####").format("SHE-OBSER", self.observation_section)
		log.info("autoname")
		log.info(self.rev_number)
		# log.info(self.eqp_number)
