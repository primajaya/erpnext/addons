// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('SHE Observation', {
	onload_post_render: function(frm){
		// if(frm.fields_dict.section_break_hazard.wrapper.find(".empty-section")){
		// 	console.log("================ onload_post_render");
			// frm.fields_dict.section_break_hazard.wrapper.removeClass("empty-section");
			// frm.fields_dict.section_break_hazard.wrapper.addClass("visible-section");
		// }
		// // frm.set_df_property("section_break_hazard", "hidden", 0);
		// if(frm.fields_dict.section_break_hazard.wrapper.find(".visible-section")){
		// 	console.log("================ onload_post_render visible-section");
		// 	// frm.fields_dict.section_break_hazard.wrapper.removeClass('empty-section');
		// 	// frm.fields_dict.section_break_hazard.wrapper.addClass('visible-section');
		// }
		
		// frm.trigger("set_rev_number");
	},
	setup: function(frm){

		let divTableHazard = $('<div class="form-column col-sm-12"></div>');
		let formGrid = $('<table class="table"></table>').appendTo(divTableHazard);
		let gridheading = $('<thead class=""></thead>').appendTo(formGrid);
		// let gridRow = $('<div class="grid-row"></div>').appendTo(gridheading);
		
		let divHeaderDataRowRow = $('<tr class=""></tr>').appendTo(gridheading);
		
		$('<th class="" colspan="2">Hazard Category</th>').appendTo(divHeaderDataRowRow);
		$('<th class="">Action</th>').appendTo(divHeaderDataRowRow);
		

		// let htmltbl = divTableHazard.append(tableHazard);
		// let htmltbl = $('<div class="form-column col-sm-12">'+
		// '<div class="form-grid">'+
		// '<div class="grid-heading-row">'+
		// '<div class="grid-row">'+ 
		// // '<div class="data-row row">'+
		// // '<div class="col grid-static-col">test</div>'+
		// // '</div>'+
		// '</div></div></div></div>');

		let html = $('<table class="table">'+
		'<thead class="thead"><tr class="">'+
		'<th class="" colspan="2">test</th>'+
		'<th class="">test</th>'+
		'</tr></thead>'+
		'<tbody class="tbody"><tr class="">'+
		'<td class="">test isi</td>'+
		'<td class="">test isi</td>'+
		'<td class="">test isi</td>'+
		'</tr></tbody></table>');
		// html.appendTo(divTableHazard);
		// divHeaderDataRowRow.appendTo(gridRow);

		let gridBody = $('<tbody class=""></tbody>').appendTo(formGrid);
		// let rows = $('<div class="rows"></div>').appendTo(gridBody);
		frappe.db.get_list('SHE Hazard Category', {
			fields: ['name','hazard_code','hazard_name','description','action'],
		}).then(records => {
			
			$.each(records, function(i, r) {
			// 	frm.doc.section_break_hazard.find('section-body')

				// let gridRowBody = $('<div class="grid-row"></div>').appendTo(rows);
				let datarowBody = $('<tr class=""></tr>').appendTo(gridBody);
				$('<td class="">'+r.hazard_code+' ('+r.hazard_name+')</td>').appendTo(datarowBody);
				$('<td class="">'+(r.description?r.description:'-')+'</td>').appendTo(datarowBody);
				$('<td class="">'+(r.action?r.action:'-')+'</td>').appendTo(datarowBody);
					
			});
		});

		// console.log(frm.fields_dict.section_break_hazard);
		// divTableHazard.appendTo(frm.fields_dict.section_break_hazard.body);
		// html.appendTo(frm.fields_dict.section_break_hazard.body);
		// frm.set_value('test', divTableHazard);
		frm.set_df_property('test','options',formGrid);
		
	},
	refresh: function(frm) {
		
		// refresh_field('section_break_hazard');
		// frm.fields_dict.section_break_hazard.wrapper.addClass("visible-section").removeClass("empty-section");
		// frm.fields_dict.section_break_hazard.wrapper.addClass("visible-section");
		
		
		// frm.fields_dict.observation_task_form.grid.grid_buttons.addClass('hidden');
		// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();

		frm.set_df_property('observation_task_form', 'cannot_delete_rows', 1);
		frm.set_df_property('observation_task_form', 'cannot_add_rows', 1);

		let me = this;
		// var exist = [];
		// if (!frm.is_new()) {
		// 	$.each(frm.doc.observation_task_form, function(i, r) { exist.push(r.name); });
		// }
		if(frm.doc.designation && frm.is_new()){
			// me.observation_task(frm);
			frm.trigger('observation_task');
		}
		if(frm.is_new()){
			frm.set_value('observation_date', moment().toDate());
		}

		frm.trigger('set_query_employee_id');
		
		// console.log(frm.fields_dict.section_break_hazard);
		
		
	},
	onload: function(frm){
		frm.set_df_property("observer_employee_id", "read_only", 1);
		console.log("user");
		console.log(frappe.session.user);
		console.log(user);
		if(frappe.session.user != "Administrator"){
			frappe.call({
				'method': 'frappe.client.get',
				args: {
					doctype: 'Employee',
					filters: {user_id: frappe.session.user},
				},
				callback: function(data) {
					console.log("data");
					console.log(data);
					let de = data.message // data employee
					if(de){
						frm.set_value({
							observer_employee_id : de.name,
							observation_section : de.department,
							branch_id : de.branch
						})
					}
				}
			});
		}
		frm.trigger("set_query_eqp");
	},

	// section_break_hazard: function(frm){
	// 	frm.fields_dict.section_break_hazard.wrapper.addClass('visible-section');
	// 	frm.fields_dict.section_break_hazard.wrapper.removeClass('empty-section');
	// },

	designation: function(frm){
		frm.trigger('observation_task');
	},

	observation_task:function(frm){
		// frm.set_value('observation_task_form', []);
		// frm.refresh_field('observation_task_form');
		// frappe.db.get_list('SHE Task Steps', {
		// 	fields: ['name','task_step_name', 'she_hazard_category_id'],
		// 	// filters: {name: ['like', frm.doc.customer + '%']}
		// 	filters: {designation: frm.doc.designation}
	frappe.call({
		method: "addons.she.doctype.she_task_steps_detail.she_task_steps_detail.get_data_by_designation",
		args: {designation: frm.doc.designation}
	}).then(records => {

			frm.doc.observation_task_form = [];
			if(records.message){
				$.each(records.message, function(i, r) {
					frm.add_child('observation_task_form', {
						she_task_steps_id: r.name,
						she_task_steps_name: r.task_step_name,
						she_task_steps_hazard_id:r.she_hazard_category_id,
						she_task_steps_hazard_code:r.she_hazard_category_code,
					});
				});
			}
			
			frm.refresh_field('observation_task_form');
			// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();
		});
		
	},
	observation_section: function(frm){
		frm.trigger("set_query_employee_id");
		// frm.trigger("set_rev_number");
	},
	set_query_employee_id: function(frm){
		if(frm.doc.observation_section){
			console.log("employee_id ")
			// set filters for Link field item_code in
			// items field which is a Child Table
			frm.set_query('employee_id', 'she_observation_employee', () => {
				return {
					filters: [
						["Employee","department","=", frm.doc.observation_section],
						["Employee","branch","=",frm.doc.branch_id],
						["Employee","name","!=", frm.doc.observer_employee_id],
					]
				}
			})
		}
	},
	branch_id: function(frm){
		// frm.trigger("set_rev_number");
		frm.trigger("set_query_eqp");
	},
	set_rev_number: function(frm){
		refresh_field('branch_id');
		console.log('frm.doc.branch_abbr');
		console.log(frm.doc.branch_abbr);
		frm.set_value('rev_number',
			(frm.doc.branch_abbr?frm.doc.branch_abbr:"")+"-"+
			moment(new Date()).format("YYMMDD")+"-"+
			(frm.doc.observation_section_abbr?frm.doc.observation_section_abbr:"")+"-"+
			frm.doc.observer_employee_id+"-#####"
		);
	},
	set_query_eqp: function(frm){
		frm.set_query('eqp_number', ()=>{
			return {
				fields: {value:'name',description:['asset_name','item_code']},
				filters: {
					branch:frm.doc.branch_id
				}
			}
		})
	}
});
