// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('SHE Inspection Parameter', {
	refresh: function(frm) {
		frm.trigger('set_detail_param_is_item');
		if(frm.doc.she_insp_param_custom_header == 'Custom'){
			frm.set_df_property('she_insp_param_table_custom_header','hidden',0);
			frm.set_df_property('she_insp_param_table_custom_header','reqd',1);
		}else{
			frm.set_df_property('she_insp_param_table_custom_header','hidden',1);
			frm.set_df_property('she_insp_param_table_custom_header','reqd',0);
		}
	},
	she_insp_param_is_item: function(frm){
		frm.set_value('she_insp_param_detail',[]);
		// frm.trigger('set_detail_param_is_item');
	},
	set_detail_param_is_item: function(frm){
		console.log("set_detail_param_is_item");
		// set_param_is_item(frm);
		
	},
	she_insp_param_custom_header: function(frm){
		if(frm.doc.she_insp_param_custom_header == 'Custom'){
			frm.set_df_property('she_insp_param_table_custom_header','hidden',0);
			frm.set_df_property('she_insp_param_table_custom_header','reqd',1);
		}else{
			// if(typeof frm.doc.she_insp_param_table_custom_header !== "undefined" && frm.doc.she_insp_param_table_custom_header.length>0){
				frm.set_value('she_insp_param_table_custom_header',[]);
			// }
			frm.set_df_property('she_insp_param_table_custom_header','hidden',1);
			frm.set_df_property('she_insp_param_table_custom_header','reqd',0);
		}
	}
});

function set_param_is_item(frm){
	var df = frappe.meta.get_docfield("SHE Inspection Parameter Detail","she_insp_det_param", frm.doc.name);
		if(frm.doc.she_insp_param_is_item){
			df.options = 'Item';
			df.fieldtype = 'Link';
		}else{
			df.fieldtype = 'Data';
			df.options = '';
		}
		frm.refresh_field('she_insp_det_param');
	// 	console.log(df);

	// frm.set_df_property("she_insp_param_detail","fieldtype",'Link',frm.doc.name,"she_insp_det_param","name_of_child_table_row")

	if(frm.doc.she_insp_param_is_item ){
		frm.add_child('she_insp_param_detail');
		frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
			'she_insp_det_param',
			'options',
			'Item'
		);
		frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
			'she_insp_det_param',
			'fieldtype',
			'Link'
		);
		
	}
	
	refresh_field('she_insp_det_param');
	frm.refresh_field('she_insp_param_detail');
};

frappe.ui.form.on('SHE Inspection Parameter Detail', {
	// she_insp_param_detail_refresh: function(frm){
	// 	set_param_is_item(frm);
	// },
	she_insp_det_param: function(frm, cdt, cdn){
		if(frm.doc.she_insp_param_is_item){
			console.log("change she_insp_det_param");
			let d = locals[cdt][cdn];

			let she_insp_det_param_data = frappe.model.get_value(cdt,cdn,'she_insp_det_param');
			frappe.db.get_value('Item',{name:she_insp_det_param_data},'item_name',function(val){
				frappe.model.set_value(cdt,cdn,'she_insp_det_sub_param_1',val.item_name);
			});
		}
		

	},
    she_insp_param_detail_add: function(frm, cdt, cdn) {
		console.log("she show chenge");
		console.log(cdt);
		console.log(cdn);
		let d = locals[cdt][cdn];
		
		// console.log(d.she_insp_det_param);
		// frm.set_df_property("child_table_field_name","options",["A","B","C","D"],current_form_name,"field_name_of_chils_table","name_of_child_table_row")
		// if(frm.doc.she_insp_param_is_item){
		// 	frm.set_df_property("she_insp_param_detail","fieldtype",'Link',frm.doc.name,"she_insp_det_param",cdn);
		// 	frm.set_df_property("she_insp_param_detail","options",'Item',frm.doc.name,"she_insp_det_param",cdn);
		// 	frm.set_df_property("she_insp_param_detail","data-target",'Item',frm.doc.name,"she_insp_det_param",cdn);
		// }else{
		// 	frm.set_df_property("she_insp_param_detail","fieldtype",'Data',frm.doc.name,"she_insp_det_param",cdn);
		// 	frm.set_df_property("she_insp_param_detail","options",'',frm.doc.name,"she_insp_det_param",cdn);
		// }
		
		// var df = frappe.meta.get_docfield("SHE Inspection Parameter Detail","she_insp_det_param", frm.doc.name);
		// if(frm.doc.she_insp_param_is_item){
		// 	df.fieldtype = 'Link';
		// 	df.options = 'Item';
		// }else{
		// 	df.fieldtype = 'Data';
		// 	df.options = '';
		// }
		// console.log(df);

		// var df = frappe.meta.get_docfield("SHE Inspection Parameter Detail","she_insp_det_param", cdn);
		if(frm.doc.she_insp_param_is_item){
			update_docfield_property_custom(frm, cdt,cdn, 'she_insp_det_param', 'fieldtype', 'Link');
			update_docfield_property_custom(frm, cdt,cdn, 'she_insp_det_param', 'options', 'Item');
		}
		// console.log(df);
		// console.log(df.user_defined_columns);
		// console.log(df.fieldname);
		// if(frm.doc.she_insp_param_is_item){
		// 	df.fieldtype = 'Link';
		// 	df.options = 'Item';
		// // 	df.target = 'Item';
		// }
		// else{
		// 	df.fieldtype = 'Data';
		// 	df.options = '';
		// }
		// this.debounced_refresh();
		// this.refresh_field("she_insp_det_param");
		// this.docfields.find((d) => d.fieldname === fieldname)[fieldtype] = 'Link';

		// if (this.user_defined_columns && this.user_defined_columns.length > 0) {
		// 	let field = this.user_defined_columns.find((d) => d.fieldname === fieldname);
		// 	if (field && Object.keys(field).includes(property)) {
		// 		field[property] = value;
		// 	}
		// }

		// this.debounced_refresh();
		// console.log(df);


		// frm.fields_dict.she_insp_param_detail.set_df_property('she_insp_det_param',
		// 'fieldtype',
		// 'Link');
		// frm.fields_dict.she_insp_param_detail.set_df_property('she_insp_det_param',
		// 'options',
		// 'Item');
		// frm.fields_dict.she_insp_param_detail.grid.update_docfield_property('she_insp_det_param',
		// 'fieldtype',
		// 'Link');
		// frm.fields_dict.she_insp_param_detail.grid.update_docfield_property('she_insp_det_param',
		// 'options',
		// 'Item');
		// console.log(frm.fields_dict.she_insp_param_detail.grid.docfields);
		// refresh_field('she_insp_param_detail');
		// refresh_field('she_insp_det_param');
		
		// set_param_is_item(frm);
		// frm.add_child('she_insp_param_detail');
		// if(frm.doc.she_insp_param_is_item){
		// 	frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
		// 		'she_insp_det_param',
		// 		'fieldtype',
		// 		'Link'
		// 	);
		// 	frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
		// 		'she_insp_det_param',
		// 		'options',
		// 		'Item'
		// 	);
			
		// }
		// else{
		// 	frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
		// 		'she_insp_det_param',
		// 		'fieldtype',
		// 		'Data'
		// 	);
		// 	frm.fields_dict.she_insp_param_detail.grid.update_docfield_property(
		// 		'she_insp_det_param',
		// 		'options',
		// 		''
		// 	);
		// }
	}
});

function update_docfield_property_custom(frm, cdt, cdn, fieldname, property, value) {
	// update the docfield of each row
	// if (!this.grid_rows) {
	// 	return;
	// }
	 

	for (let row of frm.fields_dict.she_insp_param_detail.grid.grid_rows) {
		let docfield = row.docfields.find((d) => d.fieldname === fieldname);
		if (docfield) {
			docfield[property] = value;
		} else {
			throw `field ${fieldname} not found`;
		}
	}

	// update the parent too (for new rows)
	// this.docfields.find((d) => d.fieldname === fieldname)[property] = value;
	// var df = frappe.meta.get_docfield("SHE Inspection Parameter Detail","she_insp_det_param", cdn);
	// if (frm.fields_dict.she_insp_param_detail.grid.user_defined_columns && frm.fields_dict.she_insp_param_detail.grid.user_defined_columns.length > 0) {
		let field = frm.fields_dict.she_insp_param_detail.grid.user_defined_columns.find((d) => d.fieldname === fieldname);
		// let field = frappe.meta.get_docfield(cdt, fieldname, cdn);
		// if (field && Object.keys(field).includes(property)) {
			field[property] = value;
		// }
	// }
	// frm.fields_dict.she_insp_param_detail.grid.refresh_row(cdn)

	// refresh_field(fieldname);
	// this.refresh.bind(this);
	// frappe.utils.debounce(this.debounced_refresh, 100);
	console.log(frm.fields_dict.she_insp_param_detail);
	console.log(frm.fields_dict.she_insp_param_detail.grid.grid_rows); 
	console.log(frm.fields_dict.she_insp_param_detail.grid.user_defined_columns);
}
