# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

from addons.repository.she_task_steps_detail import get_data_by_parent
import frappe
from frappe.model.document import Document

class SHETaskStepsDetail(Document):
	pass

@frappe.whitelist()
def get_data_by_designation(designation):
    return get_data_by_parent(designation)
