// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('SHE Inspection', {
	onload: function(frm){
		frm.set_df_property('she_insp_parameter_add_col','hidden',1);
		frm.set_df_property('she_insp_parameter_note','options',$('<div class="form-group"><p>Description: Determination of "SCORE" if: Criteria are not met "0", Only partially filled "1". Criteria meets "2"</p></div>'));
		
	},
	refresh: function(frm) {
		frm.set_df_property('she_insp_detail', 'cannot_delete_rows', 1);
		frm.set_df_property('she_insp_detail', 'cannot_add_rows', 1);

		frm.trigger('set_type_value_on_detail');
		if(frm.is_new()) frm.set_value('she_insp_date', moment());
		frm.set_df_property('she_insp_parameter_add_col_link','ignore_link_validation', 1);
	},
	she_insp_date: function(frm){
		if(frm.doc.she_insp_parameter_custom_header == "Weekly"){
			frm.set_value('she_insp_parameter_add_col_select',Math.ceil(moment(frm.doc.she_insp_date).date() / 7));
		}else if(frm.doc.she_insp_parameter_custom_header == "Day/Date"){
			frm.set_value('she_insp_parameter_add_col_data',moment(frm.doc.she_insp_date).format('YYYY-MM-DD'));
		}
		console.log(frm.doc.she_insp_date);
		for (let row of frm.fields_dict.she_insp_detail.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_date',frm.doc.she_insp_date);
			
		}
	},
	she_insp_parameter_add_col_select: function(frm){
		console.log("frm.doc.she_insp_parameter_add_col_select");
		console.log(frm.doc.she_insp_parameter_add_col_select);
		frm.set_value('she_insp_parameter_add_col_data',frm.doc.she_insp_parameter_add_col_select);
		for (let row of frm.fields_dict.she_insp_detail.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_header',frm.doc.she_insp_parameter_add_col_select);
			frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_header_id',frm.doc.she_insp_parameter_add_col_select);
			
		}
	},
	// edit ini
	she_insp_parameter_add_col_link: function(frm){
		frappe.call({
			method: "frappe.client.get_value",
			args:{
				doctype:'SHE Inspection Parameter Detail', ///SHE Inspection Parameter Custom Header
				fieldname: ['name', 'she_insp_det_param', 'she_insp_det_sub_param_1', 'she_insp_det_sub_param_2'], ///she_insp_param_custom_header_name
				filters:{name:frm.doc.she_insp_parameter_add_col_link},
				parent: 'SHE Inspection Parameter'
			},
			callback: function(r){
				if(r.message){
					let val = r.message;
					frm.set_value('she_insp_parameter_add_col_data',val.she_insp_det_sub_param_1);

					/// inject item to child table

					for (let row of frm.fields_dict.she_insp_detail.grid.grid_rows) {
						console.log(row);
						console.log(row.doc.doctype);
						console.log(row.doc.name);
						frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_param_id',val.name);
						frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_param',val.she_insp_det_param);
						frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_sub_param_1',val.she_insp_det_sub_param_1);
						frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_sub_param_2',val.she_insp_det_sub_param_2);
						frappe.model.set_value(row.doc.doctype,row.doc.name,'she_insp_detail_date',frm.doc.she_insp_date);
						
					}
				}
				frm.refresh_field('she_insp_detail');
			}
		});
		frm.set_df_property('she_insp_parameter_add_col_data','hidden',0);
	},
	//------
	she_insp_parameter: function(frm){
		// Get the first and last day of the month
		// const firstDay = moment(frm.doc.she_insp_date).startOf('month').date();
		// const endDay = moment(frm.doc.she_insp_date).endOf('month').date();
		// const firstWeekOfMonth = Math.ceil(firstDay/7);
		// const lastWeekOfMonth = Math.ceil(endDay/7);
		// const week = moment(frm.doc.she_insp_date).week();

		frm.toggle_display([
			'she_insp_parameter_add_col', 
			'she_insp_parameter_add_col_select', 
			'she_insp_parameter_add_col_link', 
			'she_insp_parameter_add_col_data', 
			'she_insp_detail_header', 
			
		], 0);
		frm.toggle_reqd([
			'she_insp_parameter_add_col', 
			'she_insp_parameter_add_col_select', 
			'she_insp_parameter_add_col_link', 
		], 0);

		if(frm.doc.she_insp_parameter_custom_header == "Day/Date"){
			// frm.set_df_property('she_insp_parameter_add_col','hidden',0);
			frm.set_value('she_insp_parameter_add_col',moment(frm.doc.she_insp_date));
			frm.refresh_field('she_insp_parameter_add_col');
		}else if(frm.doc.she_insp_parameter_custom_header == "Custom"){
			
			frm.set_df_property('she_insp_parameter_add_col_link','hidden',0);
			frm.set_df_property('she_insp_parameter_add_col_link','reqd',1);

			/// edit ini jadi item parameter
			frm.set_query('she_insp_parameter_add_col_link', function() {
				if(frm.doc.she_insp_parameter){
					return {
						// query: 'addons.she.doctype.she_inspection_parameter_custom_header.she_inspection_parameter_custom_header.get_query_link_combo',
						query: 'addons.she.doctype.she_inspection_parameter_detail.she_inspection_parameter_detail.get_query_link_combo',
						filters: {
							'parent': frm.doc.she_insp_parameter,
						},
						// parent: 'SHE Inspection Parameter',
						// fields: ['name','she_insp_det_param','she_insp_det_sub_param_1']
					};
				}else{
					return None;
				}
			});
			///--------

			frm.refresh_field('she_insp_parameter_add_col_link');
		}else if(frm.doc.she_insp_parameter_custom_header == "Weekly"){
			frm.set_df_property('she_insp_parameter_add_col_select','hidden',0);
			frm.set_df_property('she_insp_parameter_add_col_select','reqd',1);
			frm.set_value('she_insp_parameter_add_col_select',Math.ceil(moment(frm.doc.she_insp_date).date() / 7));
			
			frm.refresh_field('she_insp_parameter_add_col_select');
		}


		frm.set_value('she_insp_detail', []);
		frm.refresh_field('she_insp_detail');

		///edit ini ketika inspeksi custom
		if(frm.doc.she_insp_parameter_custom_header == "Custom"){
			frappe.call({
				method: 'frappe.client.get_list',
				args: {
					doctype: 'SHE Inspection Parameter Custom Header',/// Detail',
					parent: 'SHE Inspection Parameter',
					filters: {parent: frm.doc.she_insp_parameter},
					fields: ['name','she_insp_param_custom_header_name'],
					as_dict: true,
					limit_page_length: 100
				}	
			}).then(records => {
				console.log(records.message);
				frm.doc.she_insp_detail = [];
				if(records.message){
					$.each(records.message, function(i, r) {
						frm.add_child('she_insp_detail', {
							she_insp_detail_header_id: r.name,
							she_insp_detail_header: r.she_insp_param_custom_header_name,
							she_insp_detail_date:frm.doc.she_insp_date
						});
						frm.trigger('set_type_value_on_detail');
					});
					frm.refresh_field('she_insp_detail');

					frm.toggle_display([
						'she_insp_detail_header',
					], 1);
					
					frm.fields_dict.she_insp_detail.grid.update_docfield_property(
						'she_insp_detail_header',
						'in_list_view',
						1
					);
					frm.fields_dict.she_insp_detail.grid.update_docfield_property(
						'she_insp_detail_sub_param_2',
						'in_list_view',
						0
					);
					console.log('frm.fields_dict.she_insp_detail.grid.grid_rows');
					console.log(frm.fields_dict.she_insp_detail.grid.grid_rows);
					for (let row of frm.fields_dict.she_insp_detail.grid.grid_rows) {
						let docfield = row.docfields.find((d) => d.fieldname === 'she_insp_detail_header');
						console.log('docfield');
						console.log(docfield);
						if (docfield) {
							docfield.in_list_view = 1;
						} 
						// else {
						// 	throw `field she_insp_detail_header not found`;
						// }
					}
				
					let field = frm.fields_dict.she_insp_detail.grid.user_defined_columns.find((d) => d.fieldname === 'she_insp_detail_header');
					console.log('field');
					console.log(frm.fields_dict.she_insp_detail.grid);
					if(field){
						field.in_list_view = 1;
					}
					frm.refresh_field('she_insp_detail_header');
				}
				
				frm.refresh_field('she_insp_detail_header');
				frm.refresh_field('she_insp_detail_sub_param_2');
				frm.refresh_field('she_insp_detail');
				// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();
			});
		}else {
			frappe.call({
				method: 'frappe.client.get_list',
				args: {
					doctype: 'SHE Inspection Parameter Detail',
					parent: 'SHE Inspection Parameter',
					filters: {parent: frm.doc.she_insp_parameter},
					fields: ['name','she_insp_det_param','she_insp_det_sub_param_1','she_insp_det_sub_param_2'],
					as_dict: true,
					limit_page_length: 100
				}	
			}).then(records => {

				frm.doc.she_insp_detail = [];
				if(records.message){
					$.each(records.message, function(i, r) {
						frm.add_child('she_insp_detail', {
							she_insp_detail_param_id: r.name,
							she_insp_detail_param: r.she_insp_det_param,
							she_insp_detail_sub_param_1:r.she_insp_det_sub_param_1,
							she_insp_detail_sub_param_2:r.she_insp_det_sub_param_2,
							she_insp_detail_date:frm.doc.she_insp_date
						});
						frm.trigger('set_type_value_on_detail');
					});
					
				}

				frm.refresh_field('she_insp_detail');
				// frm.fields_dict.observation_task_form.grid.wrapper.find('.btn-open-row').hide();
			});
		}

		
		///----------
	},
	set_type_value_on_detail: function(frm){
		// if (typeof (frm.doc.she_insp_detail) ==="undefined"){
		// 	return
		// }
		// console.log(frm.fields_dict.she_insp_detail.grid);
		// frm.refresh_field('she_insp_detail');
		// // //for Checklist
		// if(frm.doc.she_insp_parameter_type=="Check"){
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'fieldtype',
		// 		'Check'
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'default',
		// 		0
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'label',
		// 		'Yes/No'
		// 	);
			
			
			
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_hazard',
		// 		'hidden',
		// 		1
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_hazard',
		// 		'in_list_view',
		// 		0
		// 	);
		// }else if(frm.doc.she_insp_parameter_type=="Select"){ //// for score
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'fieldtype',
		// 		'Select'
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'options',
		// 		[0,1,2]
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'default',
		// 		0
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_value',
		// 		'label',
		// 		'Score'
		// 	);
			
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_hazard',
		// 		'in_list_view',
		// 		1
		// 	);
		// 	frm.fields_dict.she_insp_detail.grid.update_docfield_property(
		// 		'she_insp_detail_hazard',
		// 		'hidden',
		// 		0
		// 	);
			
		// }

		frm.refresh_field('she_insp_detail');
	}

});
