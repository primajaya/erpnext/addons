// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

var dataHistoryList=[];
var dataIdEmpNCat='';

frappe.ui.form.on('SHE APD', {
	setup: function(frm){
		make_table_history(frm, []);
	},
	refresh: function(frm) {
		// frm.set_df_property("she_apd_accepted_date", "read_only", 1);
		// frm.toggle_enable("she_apd_accepted_date", true);
		// frm.set_df_property("she_apd_accepted_date", "hidden", 0);
		// frm.set_df_property("she_apd_next_period_alocation", "read_only", 1);
		
		frm.trigger('set_table_history_apd');
		
	},
	onload: function(frm){
		// console.log('frappe.session');
		// console.log(frappe.session);
		set_hidden_status(frm);
		// frm.set_value({
		// 	'she_apd_purpose_period':, moment().toDate());
		// })
		frm.trigger('set_query_she_apd_category_size');
		if(frm.is_new()) frm.set_value('she_apd_purpose_period', moment(new Date()));
		frm.set_df_property('she_apd_category_size_id','ignore_link_validation', 1);
		
	},
	// validate: function(frm){
	// 	if(frm.doc.she_apd_category_size_id && !frappe.db.exists('SHE APD Category Size',frm.doc.she_apd_category_size_id)){
	// 		frm.set_value('she_apd_category_size_id','');
	// 	}
		
	// },

	she_apd_category_id: function(frm){
		// frm.trigger('set_she_apd_next_period_alocation');
		frm.trigger('set_table_history_apd');
		frm.trigger('set_query_she_apd_category_size');
	},
	she_apd_purpose_period: function(frm){
		// frm.trigger('set_she_apd_next_period_alocation');
	},

	set_she_apd_next_period_alocation: function(frm){
		if (frm.doc.she_apd_purpose_period && frm.doc.she_apd_accepted_date && frm.doc.she_apd_category_id){
			frm.set_value('she_apd_next_period_alocation', moment(frm.doc.she_apd_accepted_date).add(frm.doc.she_apd_category_period,frm.doc.she_apd_category_period_cat));
		}
	},
	employee_id: function(frm){
		frm.trigger('set_table_history_apd');
	},
	set_table_history_apd: function(frm){
		if (frm.doc.employee_id && frm.doc.she_apd_category_id){
			
			get_data_list_she_apd(frm,(r)=>{
				if(r.message){
					make_table_history(frm, r.message);
					make_message_n_html_warning(frm, r.message);
				}
				
			});
				
			// frappe.call({
			// 	method:'addons.she.doctype.she_apd.she_apd.get_apd_by_cat',
			// 	args: {
			// 		employee_id: frm.doc.employee_id,
			// 		apd_cat_id: frm.doc.she_apd_category_id,
				
			// 	},
			// 	callback: function(r) {
		
			// 		if(!r.exc) {
						
			// 			console.log(r);
			// 			let result = r.message;
			// 			// make_datatable(result);
			// 			make_table_history(frm, result);
			// 		}
			// 	}
			// });
		}
	},
	she_apd_status: function(frm){
		if(!frm.doc.she_apd_category_id || !frm.doc.employee_id){
			if(frm.doc.she_apd_status !="Propose"){
				frm.set_value('she_apd_status','Propose');
				frappe.throw(__("Please set a APD Category or Employee"));
			}
		}

		if(frm.doc.she_apd_status!="Accepted"){
			frm.set_value('she_apd_accepted_date', '');
			frm.set_value('she_apd_next_period_alocation', '');
		}

		if(frm.doc.she_apd_status != "Propose" && frm.doc.she_apd_status != "Rejected"){
			get_data_list_she_apd(frm,(r)=>{
				dataHistoryList = r.message;
				if(dataHistoryList.length){
					if(dataHistoryList[0].she_apd_next_period_alocation > moment().format("YYYY-MM-DD")){
						// console.log("masuk");
						frappe.confirm(
							__("Are you sure to change status this data employee {0} apd {1} before next period alocation {2} ?",
								[frm.doc.employee_id, frm.doc.she_apd_category_name, dataHistoryList[0].she_apd_next_period_alocation]
							),
							function(){
								if(frm.doc.she_apd_status=="Accepted"){
									frm.set_value('she_apd_accepted_date',  moment());
									frm.trigger("set_she_apd_next_period_alocation");
								}
								// window.close();
							},
							function(){
								if(frm.is_new()){
									frm.set_value('she_apd_status','Propose');
								}
								frm.set_value('she_apd_accepted_date','');
								// show_alert('Thanks for continue here!');
							}
						)
						
					}
				}else{
					if(frm.doc.she_apd_status=="Accepted"){
						frm.set_value('she_apd_accepted_date',  moment());
						frm.trigger("set_she_apd_next_period_alocation");
					}
				}
			}, 1);

			// frappe.call({
			// 	'method': 'frappe.client.get_list',
			// 	args: {
			// 		doctype: 'SHE APD',
			// 		filters: {
			// 			employee_id: frm.doc.employee_id,
			// 			she_apd_category_id: frm.doc.she_apd_category_id,
			// 			she_apd_status: 'Accepted'
			// 		},
			// 		fields:['name','she_apd_accepted_date','she_apd_next_period_alocation', 'she_apd_purpose_period'],
			// 		order_by:'she_apd_accepted_date desc',
			// 		limit_page_length:1,
			// 		as_list:true,
			// 	},
			// 	callback: function(r) {
			// 		if(r.message){
			// 			console.log("she_apd_next_period_alocation "+r.message[0].she_apd_next_period_alocation);
			// 			console.log("now "+moment().format("YYYY-MM-DD"));
			// 			if(r.message[0].she_apd_next_period_alocation > moment().format("YYYY-MM-DD")){
			// 				console.log("masuk");
			// 				frappe.confirm(
			// 					__("Are you sure to change status this data employee {0} apd {1} before next period alocation {2} ?",
			// 						[frm.doc.employee_id, frm.doc.she_apd_category_name, r.message[0].she_apd_next_period_alocation]
			// 					),
			// 					function(){
			// 						if(frm.doc.she_apd_status=="Accepted"){
			// 							frm.set_value('she_apd_accepted_date',  moment());
			// 							frm.trigger("set_she_apd_next_period_alocation");
			// 						}
			// 						// window.close();
			// 					},
			// 					function(){
			// 						if(frm.is_new()){
			// 							frm.set_value('she_apd_status','');
			// 						}
			// 						frm.set_value('she_apd_accepted_date','');
			// 						// show_alert('Thanks for continue here!');
			// 					}
			// 				)
							
			// 			}
			// 		}
			// 	}
			// });
		}
		
		
	},
	set_query_she_apd_category_size:function(frm){
		frm.set_query('she_apd_category_size_id', function() {
			if(frm.doc.she_apd_category_id){
				return {
					query: "addons.she.doctype.she_apd_category_size.she_apd_category_size.apd_category_size_query",
					filters: {
						parent: frm.doc.she_apd_category_id
					},
				};
			}else{
				return None;
			}
		});
		frm.refresh_field("she_apd_category_size_id");
	},
	she_apd_category_size_id: function(frm){
		// validate select she_apd_category_size_id
		if(frm.doc.she_apd_category_size_id ){ 
			frappe.call({
				method: "addons.she.doctype.she_apd_category_size.she_apd_category_size.get_data_she_apd_cat_size",
				args:{
					name: frm.doc.she_apd_category_size_id,
					parent: frm.doc.she_apd_category_id?frm.doc.she_apd_category_id:null
					
				},
				kwargs:{
					ignore_permissions: true
				},
				callback: function(r) {
					if(r.message){
						frm.set_value('she_apd_category_size_id',r.message.name);
						frm.set_value('she_apd_category_size_name',r.message.she_apd_category_size_number);
					}else{
						frm.set_value('she_apd_category_size_id','');
					}
				}
			});
			
			
			
		}else{
			frm.set_value('she_apd_category_size_name','');
		}
	}
	

});

function get_data_list_she_apd(frm, callback, limit_page_length=5, order_by='she_apd_accepted_date desc'){
	
	// console.log("get_data_list_she_apd");
	// if(dataIdEmpNCat != (frm.doc.employee_id+frm.doc.she_apd_category_id) || dataHistoryList.length<=0){
		frappe.call({
			'method': 'frappe.client.get_list',
			args: {
				doctype: 'SHE APD',
				filters: {
					employee_id: frm.doc.employee_id,
					she_apd_category_id: frm.doc.she_apd_category_id,
					she_apd_status: 'Accepted',
					she_apd_accepted_date: ['<=', frm.doc.she_apd_purpose_period],
					name: ['!=', frm.doc.name]
				},
				fields:['name','she_apd_accepted_date','she_apd_next_period_alocation', 'she_apd_purpose_period'],
				order_by:order_by,
				limit_page_length:limit_page_length,
				as_list:true,
			},
			callback: callback
			//  function(r) {
			
			// 	if(r.message){
			// 		data = r.message;
			// 	}else{
			// 		data = [];
			// 	}
			// 	console.log("data isi");
			// 	console.log(data);
			// }
		});
	// }else{
	
	// }
}

function set_hidden_status(frm){
	if(frappe.session.user == "Administrator"){
		if (frm.is_new()){
			frm.set_value('she_apd_status','Propose');
		}
		frm.set_df_property('she_apd_status','hidden', 0);
		frm.set_df_property('she_apd_note','hidden', 0);
	}else{
		frappe.call({
			'method': 'frappe.client.get',
			args: {
				doctype: 'Employee',
				filters: {user_id: frappe.session.user},
			},
			callback: function(data) {
				let de = data.message // data employee
				if(de){
					if (frm.is_new()){
						frm.set_value('she_apd_status','Propose');
					}
					if(de.designation.toLowerCase() != "operator"){
						frm.set_df_property('she_apd_status','hidden',0);
						frm.set_df_property('she_apd_note','hidden', 0);
					}

				}
			}
		});
	}
}

function make_message_n_html_warning(frm, data){
	if(frm.doc.employee_id && frm.doc.she_apd_category_id){
		if(data.length>0){
			if(data[0].she_apd_next_period_alocation > moment().format("YYYY-MM-DD")){
				
				let she_apd_html = $('<div calss="form-group"><p style="color: red;">This data employee '+frm.doc.employee_id+
								' apd '+frm.doc.she_apd_category_name+
								' before next period alocation '+data[0].she_apd_next_period_alocation+'</p></div>');
				
				frm.set_df_property('she_apd_html_message','options',she_apd_html);

				frappe.throw(
					__("This data employee {0} apd {1} before next period alocation {2} ?",
						[frm.doc.employee_id, frm.doc.she_apd_category_name, data[0].she_apd_next_period_alocation]
					)
				);
			}else{
				frm.set_df_property('she_apd_html_message','options','');
			}
		}else{
			frm.set_df_property('she_apd_html_message','options','');
		}
	}else{
		frm.set_df_property('she_apd_html_message','options','');
	}
}

function make_table_history(frm, data){
	let divTable = $('<div class="form-column col-sm-12"></div>');
	let table = $('<table class="table"></table>').appendTo(divTable);
	let tableheading = $('<thead class=""></thead>').appendTo(table);
	// let gridRow = $('<div class="grid-row"></div>').appendTo(gridheading);
	
	let HeaderDataRow = $('<tr class=""></tr>').appendTo(tableheading);
	
	$('<th class="">APD Purpose Date</th>').appendTo(HeaderDataRow);
	$('<th class="">APD Accepted Date</th>').appendTo(HeaderDataRow);
	$('<th class="">APD Next Period Alocation Date</th>').appendTo(HeaderDataRow);

	let tableBody = $('<tbody class=""></tbody>').appendTo(table);

	if(data.length<=0){
		let datarowBody = $('<tr class=""></tr>').appendTo(tableBody);
		$('<td class="text-center" colspan="3"> No Data </td>').appendTo(datarowBody);
	}else{
		$.each(data, function(i, r) {
			let datarowBody = $('<tr class=""></tr>').appendTo(tableBody);
			$('<td class="">'+r.she_apd_purpose_period+'</td>').appendTo(datarowBody);
			$('<td class="">'+r.she_apd_accepted_date+'</td>').appendTo(datarowBody);
			$('<td class="">'+r.she_apd_next_period_alocation+'</td>').appendTo(datarowBody);
			// $('<td class="">'+r[1]+'</td>').appendTo(datarowBody);
			// $('<td class="">'+r[2]+'</td>').appendTo(datarowBody);
			// $('<td class="">'+r[3]+'</td>').appendTo(datarowBody);
				
		});
	}

	frm.set_df_property('she_apd_history_apd_by_cat','options',table);
}