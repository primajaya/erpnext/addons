# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

from erpnext.controllers.queries import get_fields
import frappe
from frappe.model.document import Document

class SHEAPD(Document):
	pass

@frappe.whitelist()
def get_apd_by_cat(employee_id, apd_cat_id):
    return frappe.db.get_list("SHE APD",
		filters={
			'employee_id': employee_id,
			'she_apd_category_id': apd_cat_id,
			'she_apd_status': 'Accepted'
		},
		fields=['name','she_apd_accepted_date','she_apd_next_period_alocation', 'she_apd_purpose_period'],
		order_by='she_apd_accepted_date desc',
		page_length=5,
		as_list=True,
	)