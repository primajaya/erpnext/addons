// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('SHE Green Card Digital', {
	refresh: function(frm) {
		frm.trigger('set_gdc_image_reqd');
	},
	she_gcd_status: function(frm){
		frm.trigger('set_gdc_image_reqd');
	},
	set_gdc_image_reqd: function(frm){
		frm.set_df_property('she_gdc_image_before','reqd',1);
		frm.set_df_property('she_gdc_image_after','reqd',0);
		frm.set_df_property('she_gcd_estimation_date','hidden',1);
		frm.set_df_property('she_gcd_estimation_date','reqd',0);
		frm.set_df_property('she_gcd_status_description','reqd',0);
		if(frm.doc.she_gcd_status == "Closed"){
			// frm.set_df_property('she_gdc_image_before','reqd',1);
			frm.set_df_property('she_gdc_image_after','reqd',1);
		}
		if(frm.doc.she_gcd_status == "Other"){
			frm.set_df_property('she_gcd_status_description','reqd',1);
		}
		// else if(frm.doc.she_gcd_status == "Open" ){
			// frm.set_df_property('she_gdc_image_before','reqd',1);
		// }
		
		if(frm.doc.she_gcd_status == "Progress"){
			frm.set_df_property('she_gcd_estimation_date','hidden',0);
			frm.set_df_property('she_gcd_estimation_date','reqd',1);
		}
	},
	she_gdc_image_after: function(frm){
		if(!frm.doc.she_gdc_image_before){
			frm.set_df_property('she_gdc_image_after','options','');
			frappe.throw("Please insert image before first");
		}
	}
});
