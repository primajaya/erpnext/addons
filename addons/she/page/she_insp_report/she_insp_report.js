frappe.pages['she-insp-report'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Inspection Report',
		single_column: true
	});

	let list = $(frappe.render_template('<div id="list-dormitory-inspection" class="table-area"></div>')).appendTo(page.main);

	page.inspection_parameter_field = page.add_field({
		fieldname: 'parameter',
		label: 'Inspection Paramater',
		fieldtype:'Link',
		options:"SHE Inspection Parameter",
		change: function() {
			get_data();
		}
	});
	page.inspection_date_field = page.add_field({
		fieldname: 'inspection_date',
		label: 'inspection Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});


	function get_data(){
		let me = this
		frappe.call({
			method:'addons.she.page.she_insp_report.she_insp_report.get_inspection_report',
			args: {
				parameter: page.inspection_parameter_field.get_value(),
				tgl: page.inspection_date_field.get_value(),
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					// console.log(r);
					let result = r.message;
					make_table(result);
				}
			}
		});
	}

	function make_table(data){
		// console.log(data);
		list.html("");
		$(frappe.render_template("she_insp_report_table",{data:data})).appendTo(list);
	}

}