
import frappe
from frappe import _
from addons.utils.logger import get_logger
from frappe.utils import (
    get_first_day,
	get_last_day,
)

log = get_logger(__name__)

@frappe.whitelist()
def get_inspection_report(parameter, tgl):
    data = {
        'header':[
            # {'name':'Parameter','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Description','rowspan':rowspanHeader,'colspan':0}, 
            # {'name':'Point','rowspan':rowspanHeader,'colspan':0}
            ],
        'content':'',
        'footer':{'footone':[],'foottwo':[]}
        }
    
    inspection_parameter = frappe.db.get_value("SHE Inspection Parameter",parameter,"*",as_dict=True)
    
    if not inspection_parameter:
        return data
    
    
    inspection_parameter_type = inspection_parameter.she_insp_param_custom_header
    
    queryHeaderWhere = """ where 1=1 """
    valuesHeader = {}
    headers = [
        {'name':'Score'}
    ]
    
    
    
    if inspection_parameter_type == "Custom":
        queryHeaderWhere += """ and parent=%(parameter)s """
        valuesHeader['parameter'] = parameter
        
        queryHeader = """ select *
                    from `tabSHE Inspection Parameter Custom Header`
                    """+ queryHeaderWhere +""" 
                    order by idx ASC """
        headers = frappe.db.sql(queryHeader,values=valuesHeader,as_dict=True)
        
    
    elif inspection_parameter_type == "Weekly":
        headers = []
        for x in range(1,6):
            headers.append({'name':x})
    
    log.info(headers)
    
    tblHeader = [
            {'name':'Parameter','rowspan':0,'colspan':0}, 
            {'name':'Sub Parameter','rowspan':0,'colspan':0}]
    
    
    where = """ where 1=1 """
    values = {}
    
    field = """ 
            sipd.she_insp_det_param
            ,(case 
                when sipd.she_insp_det_sub_param_1 is null then ''
                else sipd.she_insp_det_sub_param_1
            end) as she_insp_det_sub_param_1
            
            """
    fromtbl = """ from `tabSHE Inspection Parameter Detail` as sipd """
    joining = """ """
    where +=""" and sipd.parent=%(parameter)s"""
    values['parameter'] = parameter
    values['tgl'] = tgl
    
    for header in headers:
        if inspection_parameter_type=="Custom" :
            header['id'] = header.name
            header['name'] = header.she_insp_param_custom_header_name
        else:
            header['id'] = str(header['name'])
            
        header['colspan'] =0
        header['rowspan'] =0
            
        tblHeader.append(header)
        
        id = header['id']
        tblas = "tl_"+id
        fieldas = "field_"+id
        header_custom = "custom_"+id
        field +=""" ,(
            case 
                when """+tblas+""".she_insp_detail_value is null then 0 
                else """+tblas+""".she_insp_detail_value
            end) as """+fieldas
            
        joining +="""
            left join 
                `tabSHE Inspection Detail` as """+tblas+""" ON """+tblas+""".she_insp_detail_param_id=sipd.name """
        
        if inspection_parameter_type == "Weekly":
            
            firstDateMonth = get_first_day(tgl)
            lastDateMonth = get_last_day(tgl)
            log.info("month ================= ")
            log.info(firstDateMonth)
            log.info(lastDateMonth)
            joining +=""" and """+tblas+""".she_insp_detail_header_id =%("""+header_custom+""")s 
             and """+tblas+""".she_insp_detail_date >=%(firstDateMonth)s 
             and """+tblas+""".she_insp_detail_date <=%(lastDateMonth)s 
             """
            values[header_custom] = id
            values['firstDateMonth'] = firstDateMonth.strftime("%Y-%m-%d")
            values['lastDateMonth'] = lastDateMonth.strftime("%Y-%m-%d")
        else:
            joining +=""" and """+tblas+""".she_insp_detail_date =%(tgl)s """
                
        if inspection_parameter_type == "Custom":
            # joining +=""" and """+tblas+""".she_insp_detail_header_id= %("""+header_custom+""")s """
            joining +=""" and """+tblas+""".she_insp_detail_param_id= %("""+header_custom+""")s """
            values[header_custom] = id
        
        
    (data['header']).append(tblHeader)
    query="""
        select """+field+"""
            
        """+fromtbl+joining+where+"""

        order by sipd.idx ASC
    """ 
    log.info(query)
    log.info(values)
    log.info(parameter)
    log.info(tgl)
    
    
    content = frappe.db.sql( query=query
    ,values=values
    ,as_dict=False
    # ,as_dict=True
    )
    
    data['content'] = content #"sdf"
        
    log.info(data)
    
    foot1 = []
    foot2 = []
    idxcount=2
    for i,vals in enumerate(content):
        for index,val in enumerate(vals):
            # val = int(val)
            if index>=idxcount:
                if i==0:
                    foot1.append(int(val))
                elif len(foot1)>0:
                    isi = int(foot1[index-idxcount])
                    isi +=int(val)
                    foot1[index-idxcount] = isi

    for val in foot1:
        if int(foot1[0])==0:
            foot2.append(0*100)
        else:
            foot2.append((val/foot1[0])*100)

    (data['footer'])['footone'] = foot1
    (data['footer'])['foottwo'] = foot2

    
    return data