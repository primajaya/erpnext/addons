import os
import logging


def get_logger(name):
    logger = logging.getLogger(name)
    log_handler = os.environ.get('LOG_HANDLER', 'STREAM')
    log_filename = os.environ.get('LOG_FILE', 'file.log')
    # default_format = logging.Formatter('%(asctime)s - %(name)s [%(filename)s:%(lineno)d] - %(levelname)s - %(message)s')
    default_format = logging.Formatter('%(asctime)s - %(levelname)s - [%(filename)s:%(lineno)d] - %(message)s')


    # d_handler = logging.FileHandler(log_filename) if log_handler == "FILE" else logging.StreamHandler()
    # d_handler.setLevel(logging.DEBUG)
    # d_handler.setFormatter(default_format)

    i_handler = logging.FileHandler(log_filename) if log_handler == "FILE" else logging.StreamHandler()
    i_handler.setLevel(logging.INFO)
    i_handler.setFormatter(default_format)

    w_handler = logging.FileHandler(log_filename) if log_handler == "FILE" else logging.StreamHandler()
    w_handler.setLevel(logging.WARNING)
    w_handler.setFormatter(default_format)

    e_handler = logging.FileHandler(log_filename) if log_handler == "FILE" else logging.StreamHandler()
    e_handler.setLevel(logging.ERROR)
    e_handler.setFormatter(default_format)

    # logger.addHandler(d_handler)
    logger.addHandler(i_handler)
    logger.addHandler(w_handler)
    logger.addHandler(e_handler)
    logger.setLevel(logging.DEBUG)
    return logger