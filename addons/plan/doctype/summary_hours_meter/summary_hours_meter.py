# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class SummaryHoursMeter(Document):
	pass

@frappe.whitelist()
def update_data_by_asset(asset, real, current):
    data = frappe.db.get_value(
        "Summary Hours Meter",
        filters={
			'summary_asset_hm': asset
		},
        as_dict=True
        
        )
    if(data):
        frappe.db.set_value(
            "Summary Hours Meter",
            data.name,
            {
				'summary_cur_hm':current,
				'summary_real_hm':real
			}
            )
    else:
        data = frappe.new_doc("Summary Hours Meter")
        data.summary_asset_hm=asset
        data.summary_cur_hm=current
        data.summary_real_hm=real
        data.save()
