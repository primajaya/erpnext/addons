# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class HoursMeter(Document):
	pass

@frappe.whitelist()
def get_data_last_by_asset(hm_date, hm_asset):
	# values = {'hm_date':hm_date, 'hm_asset':hm_asset}
	# sql = """ SELECT name, MAX(plan_hm_date), plan_hm_real
	# 	FROM `tabHours Meter` 
	# 	WHERE plan_hm_date < %(hm_date)s and plan_hm_asset = %(hm_asset)s
	# 	ORDER BY plan_hm_date DESC """
	# return frappe.db.sql(sql,values, as_dict=1)

	return frappe.db.get_value(
		doctype="Hours Meter",
		fieldname=['name','max(plan_hm_date)','plan_hm_real','plan_hm_end'],
		filters={
			'plan_hm_date':['<',hm_date],
			'plan_hm_asset':hm_asset
		},
		order_by='plan_hm_date desc',
		as_dict=1
	)
