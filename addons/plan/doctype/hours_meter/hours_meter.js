// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Hours Meter', {
	// refresh: function(frm) {

	// },
	plan_hm_asset:function(frm){
		// frm.trigger('setValplan_hm_real');
		frappe.call({
			async:false,
			method: "frappe.client.get_value",
			args: {
				doctype:'Summary Hours Meter',
				fieldname: ['name', 'summary_real_hm', 'summary_cur_hm'],
				filters:{summary_asset_hm: frm.doc.plan_hm_asset}
			},
			callback: function (r) {
				console.log(r);
				if (!r.exc) {
					let valReal=0;
					let valCur=0;
					if(r.message.summary_real_hm != null){
						valReal = r.message.summary_real_hm;
					}
					if(r.message.summary_cur_hm != null){
						valCur = r.message.summary_cur_hm;
					}
					frm.set_value("plan_hm_smry_real_hm", valReal);
					frm.set_value("plan_hm_real", valReal);
					frm.set_value("plan_hm_smry_cur_hm", valCur);
					frm.set_value("plan_hm_start", valCur);
				}
			}
		});
		
	},
	plan_hm_start: function(frm){
		frm.trigger('calWH');
	},
	plan_hm_end: function(frm){
		frm.trigger('calWH');
	},
	before_save: function(frm){
		// if(frm.is_new()){
		// 	frappe.call({
		// 		async:false,
		// 		method: "addons.plan.doctype.replacement_hours_meter.replacement_hours_meter.update_status_data_last_by_asset",
		// 		args: {
		// 			hm_date: frm.doc.plan_hm_date,
		// 			hm_asset: frm.doc.plan_hm_asset
		// 		},
		// 		callback: function (r) {
		// 			console.log(r);
		// 			if (!r.exc) {
						
		// 			}
		// 		}
		// 	});
		// }
	},
	validate: function(frm) {
        frm.trigger('validateHM');
		frm.trigger('validatHMWH24');
    },
	validateHM: function(frm){
		if(frm.doc.plan_hm_end < frm.doc.plan_hm_start) {
            frappe.msgprint({
                 title: __("Invalid Value"),
                 indicator: "red",
                 message: __("Start Hour Meter can not be greater than End Hour Meter"),
               });
            validated = false;
        }
	},
	validatHMWH24: function(frm){
		if(frm.doc.plan_hm_work_hours > 24){
			frappe.msgprint({
				title: __("Invalid Value"),
				indicator: "red",
				message: __("Hour Meter work hours can not more 24 hours"),
			  });
		   validated = false;
		}
	},
	calWH: function(frm){
		if(frm.doc.plan_hm_start && frm.doc.plan_hm_end){
			frm.trigger('validateHM');
			if(frm.doc.plan_hm_end > frm.doc.plan_hm_start){
				frm.set_value('plan_hm_work_hours', (frm.doc.plan_hm_end - frm.doc.plan_hm_start));
				
				frm.trigger('validatHMWH24');
				frm.trigger('setValplan_hm_real');
			}
			
		}
	},
	setValplan_hm_real: function(frm){
		//get data latest HM by asset id but not this/now id and above this date
		let valPlanHMReal = frm.doc.plan_hm_real;

		// if(frm.doc.plan_hm_asset ){
		// 	frappe.call({
		// 		async:false,
		// 		method: "addons.plan.doctype.hours_meter.hours_meter.get_data_last_by_asset",
		// 		args: {
		// 			hm_date: frm.doc.plan_hm_date,
		// 			hm_asset: frm.doc.plan_hm_asset,
		// 		},
		// 		callback: function (r) {
		// 			console.log(r);
		// 			if (!r.exc) {
		// 				if(r.message.plan_hm_real){
		// 					frm.set_value('plan_hm_real',r.message.plan_hm_real);
		// 					valPlanHMReal = r.message.plan_hm_real;
		// 				}
		// 				if(r.message.plan_hm_end){
		// 					frm.set_value('plan_hm_start',r.message.plan_hm_end);
		// 				}
		// 			}
		// 		}
		// 	});
	
		// 	frappe.call({
		// 		async:false,
		// 		method: "addons.plan.doctype.replacement_hours_meter.replacement_hours_meter.get_data_last_by_asset",
		// 		args: {
		// 			hm_date: frm.doc.plan_hm_date,
		// 			hm_asset: frm.doc.plan_hm_asset
		// 		},
		// 		callback: function (r) {
		// 			console.log(r);
		// 			if (!r.exc) {
		// 				if(r.message.hm_replace_hm_real){
		// 					frm.set_value('plan_hm_real',r.message.hm_replace_hm_real);
		// 					valPlanHMReal = r.message.hm_replace_hm_real;
		// 				}
		// 				if(r.message.hm_replace_hm_new){
		// 					frm.set_value('plan_hm_start',r.message.hm_replace_hm_new);
		// 				}
		// 			}
		// 		}
		// 	});
		// }

		if(frm.doc.plan_hm_start && frm.doc.plan_hm_end ){
			
			if(valPlanHMReal==0){
				valPlanHMReal = frm.doc.plan_hm_end;
			}else{
				valPlanHMReal += frm.doc.plan_hm_work_hours;
			}

			frm.set_value('plan_hm_real',valPlanHMReal);
		}
		
	},
	after_save: function(frm){
		frappe.call({
			async:false,
			method: "addons.plan.doctype.summary_hours_meter.summary_hours_meter.update_data_by_asset",
			args: {
				asset: frm.doc.plan_hm_asset,
				real: frm.doc.plan_hm_real,
				current: frm.doc.plan_hm_end

			},
			callback: function (r) {
				console.log(r);
				if (!r.exc) {
					
				}
			}
		});
	}
});
