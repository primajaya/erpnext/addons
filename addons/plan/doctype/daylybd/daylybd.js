// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('DaylyBD', {
	

	refresh: function(frm) {
		// frm.set_df_property("bd_unit_number", "hidden", 0 );
		// frm.set_df_property("asset_egi", "hidden", 0 );
		// frm.set_df_property("asset_unit_model", "hidden", 0 );
		// frm.set_df_property("bd_trouble_date", "hidden", 0 );
		// frm.set_df_property("bd_trouble_time", "hidden", 0 );

		frm.set_df_property('bd_delay_hours', 'cannot_delete_rows', 1);
		frm.set_df_property('bd_delay_hours', 'cannot_add_rows', 1);

		frm.set_query('bd_subcomponent', function() {
			if(frm.doc.bd_component){
				return {
					filters: {
						'plan_component_id': frm.doc.bd_component,
					}
				};
			}else{
				return None;
			}
		});
	},
	before_save: function(frm){
		for (let row of frm.fields_dict.bd_mechanic.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'dbd_mech_date',frm.doc.bd_report_date);
		}
		// let calTotalHours = 0;
		for (let row of frm.fields_dict.bd_delay_hours.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'dbd_dh_date',frm.doc.bd_report_date);
			// calTotalHours +=row.doc.dbd_dh_hours;
		}
		// frm.set_value("bd_delay_total_hours",calTotalHours);
		
		frappe.call({
			async:false,
			method: "addons.plan.doctype.work_order_maintenance.work_order_maintenance.update_data_wo_from_bd",
			args: {
				data: frm.doc,
			},
			callback: function (r) {
				console.log(r);
				console.log(r.message.name);
				if (!r.exc) {
					// frm.set_value("id_dailyBD",r.message.name);
					if (r.message) {
						frappe.show_alert({
							message: __("Auto update WO Maintenance success!"),
							indicator: "blue",
						});
						// iddb = r.message.docname;
						// cur_dialog.hide();
					}
				}
				console.log(iddb);
			},
			error: function(e){
				console.log(e);
			}
		});
	},
	after_save: function(frm){
		// if(frm.is_new()){
		// 	frappe.db.get_list('DaylyBD', {
		// 		filters: {
		// 			bd_worker_order_number: frm.doc.bd_worker_order_number,
		// 		}
		// 	}).then(function(data){
		// 		if(data){
		// 			if(data.length > 0){
						
		// 			}
		// 		}
		// 	});
		// }
		
		// update data wo
		// frappe.call({
		// 	method: "addons.plan.doctype.work_order_maintenance.work_order_maintenance.update_data_wo_from_bd",
		// 	args: {
		// 		data: frm.doc,
		// 	},
		// 	callback: function (r) {
		// 		console.log(r);
		// 		console.log(r.message.name);
		// 		if (!r.exc) {
		// 			// frm.set_value("id_dailyBD",r.message.name);
		// 			if (r.message) {
		// 				frappe.show_alert({
		// 					message: __("Auto update WO Maintenance success!"),
		// 					indicator: "blue",
		// 				});
		// 				// iddb = r.message.docname;
		// 				// cur_dialog.hide();
		// 			}
		// 		}
		// 		console.log(iddb);
		// 	},
		// 	error: function(e){
		// 		console.log(e);
		// 	}
		// });
	},

	bd_worker_order_number: function(frm){

		// set data table mechanic
		frappe.db.get_list('DaylyBD Mechanic', {
			fields: ['employee_id','dbd_mech_name'],
			filters: {
				parent: frm.doc.bd_worker_order_number,
				parenttype: 'Work Order Maintenance',
				parentfield: 'bd_mechanic'
			}
		}).then(function(data) {

			// set null table mechanic 
			frm.doc.bd_mechanic = [];
			if (data){
				data.forEach(function(v) {

					// inject item to child table
					frm.add_child('bd_mechanic', {
						employee_id: v.employee_id,
						dbd_mech_name: v.dbd_mech_name,
						dbd_mech_date: frm.doc.bd_report_date
					});
					
				});
				frm.refresh_field('bd_mechanic');
			}
			
		});

		// set data table delay hourse
		frappe.db.get_list('DaylyBD Delay Hours', {
			fields: ['dbd_dh_delay_id','dbd_dh_abbr','dbd_dh_desc','dbd_dh_hours'],
			filters: {
				parent: frm.doc.bd_worker_order_number,
				parenttype: 'Work Order Maintenance',
				parentfield: 'bd_delay_hours'
			}
		}).then(function(data) {

			// set null table mechanic 
			frm.doc.bd_delay_hours = [];
			if (data){
				data.forEach(function(v) {

					// inject item to child table
					frm.add_child('bd_delay_hours', {
						dbd_dh_delay_id: v.dbd_dh_delay_id,
						dbd_dh_abbr: v.dbd_dh_abbr,
						dbd_dh_desc: v.dbd_dh_desc,
						dbd_dh_hours: v.dbd_dh_hours,
						dbd_dh_date: frm.doc.bd_report_date
					});
					
				});
				frm.refresh_field('bd_delay_hours');
			}
			
		});

	}

	
});

frappe.ui.form.on('DaylyBD Delay Hours', {
	dbd_dh_hours: function(frm, cdt, cdn){
		console.log("dbd_dh_hours in child");
		let calTotalHours = 0;
		for (let row of frm.fields_dict.bd_delay_hours.grid.grid_rows) {
			calTotalHours +=row.doc.dbd_dh_hours;
		}
		frm.set_value("bd_delay_total_hours",calTotalHours);
	},
});