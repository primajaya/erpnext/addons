# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from copy import copy

import json
from addons.utils.logger import get_logger


log = get_logger(__name__)

class DaylyBD(Document):
	pass

@frappe.whitelist()
# @frappe.validate_and_sanitize_search_inputs
def auto_create_daylybd(data):

	if isinstance(data, str):
		data = json.loads(data)
  
	log.info("=========== data ========= ")
	log.info(data)
  
	data = frappe._dict(data)
	
	data['bd_worker_order_number']= copy(data['name'])
	data['name'] = None
	data['doctype'] = "DaylyBD"
	log.info(len(data['bd_mechanic']))
	if len(data['bd_mechanic']) > 0 :
		for i in range(len(data['bd_mechanic'])):
			log.info(data['bd_mechanic'][i])
			data['bd_mechanic'][i]['name'] = None
			data['bd_mechanic'][i]['parenttype'] = data['doctype']
			data['bd_mechanic'][i]['parent'] = None
	
	if len(data['bd_delay_hours']) > 0 :
		for i in range(len(data['bd_delay_hours'])):
			log.info(data['bd_delay_hours'][i])
			data['bd_delay_hours'][i]['name'] = None
			data['bd_delay_hours'][i]['parenttype'] = data['doctype']
			data['bd_delay_hours'][i]['parent'] = None

	
    
    
	log.info("====== data after ======")
	log.info(data)

	frappe.set_value("Work Order Maintenance",data['bd_worker_order_number'],'bd_auto_create_bd',0)
	
	
	return frappe.get_doc(data).insert()
 
