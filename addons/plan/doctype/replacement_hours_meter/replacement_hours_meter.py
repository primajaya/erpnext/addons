# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class ReplacementHoursMeter(Document):
	pass

@frappe.whitelist()
def get_data_last_by_asset(hm_date,hm_asset):
    return frappe.db.get_value(
		doctype="Replacement Hours Meter",
		fieldname=['name',"max(hm_replace_date)",'hm_replace_hm_real', 'hm_replace_hm_new'],
		filters={
			'hm_replace_date':['<=',hm_date],
			'hm_replace_asset': hm_asset,
			'hm_replace_status': 0
		},
		order_by='hm_replace_date desc',
		as_dict=1
	)
    
@frappe.whitelist()
def update_status_data_last_by_asset(hm_date,hm_asset):
    data = frappe.db.get_value(
		doctype="Replacement Hours Meter",
		fieldname=['name',"max(hm_replace_date)",'hm_replace_hm_real'],
		filters={
			'hm_replace_date':['<=',hm_date],
			'hm_replace_asset': hm_asset,
			'hm_replace_status': 0
		},
		order_by='hm_replace_date desc',
		as_dict=1
	)
    
    if(data):
    	frappe.db.set_value("Replacement Hours Meter",data.name,'hm_replace_status',1)