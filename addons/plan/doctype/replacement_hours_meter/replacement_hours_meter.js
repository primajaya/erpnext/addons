// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt

frappe.ui.form.on('Replacement Hours Meter', {
	// refresh: function(frm) {

	// },
	hm_replace_asset: function(frm){
		// frappe.call({
		// 	async:false,
		// 	method: "addons.plan.doctype.hours_meter.hours_meter.get_data_last_by_asset",
		// 	args: {
		// 		hm_date: frm.doc.hm_replace_date,
		// 		hm_asset: frm.doc.hm_replace_asset,
		// 	},
		// 	callback: function (r) {
		// 		console.log(r);
		// 		if (!r.exc) {
		// 			if(r.message.plan_hm_real != null){
		// 				frm.set_value("hm_replace_hm_real", r.message.plan_hm_real);
		// 			}
		// 			if(r.message.plan_hm_end != null){
		// 				frm.set_value("hm_replace_hm_old", r.message.plan_hm_end);
		// 			}
		// 		}
		// 	}
		// });

		// if(frm.doc.hm_replace_hm_real==0){
		// 	frm.set_value("hm_replace_hm_real", frm.doc.hm_replace_hm_old);
		// }

		frappe.call({
			async:false,
			method: "frappe.client.get_value",
			args: {
				doctype:'Summary Hours Meter',
				fieldname: ['name', 'summary_real_hm', 'summary_cur_hm'],
				filters:{summary_asset_hm: frm.doc.hm_replace_asset}
			},
			callback: function (r) {
				console.log(r);
				if (!r.exc) {
					let valReal=0;
					let valCur=0;
					if(r.message.summary_real_hm != null){
						valReal = r.message.summary_real_hm;
					}
					if(r.message.summary_cur_hm != null){
						valCur = r.message.summary_cur_hm;
					}
					frm.set_value("hm_replace_smry_real_hm", valReal);
					frm.set_value("hm_replace_hm_real", valReal);
					frm.set_value("hm_replace_smry_cur_hm", valCur);
					frm.set_value("hm_replace_hm_old", valCur);
				}
			}
		});
	},
	after_save: function(frm){
		frappe.call({
			async:false,
			method: "addons.plan.doctype.summary_hours_meter.summary_hours_meter.update_data_by_asset",
			args: {
				asset: frm.doc.hm_replace_asset,
				real: frm.doc.hm_replace_hm_real,
				current: frm.doc.hm_replace_hm_new

			},
			callback: function (r) {
				console.log(r);
				if (!r.exc) {
					
				}
			}
		});
	}
});
