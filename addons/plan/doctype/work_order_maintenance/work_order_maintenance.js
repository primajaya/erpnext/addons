// Copyright (c) 2024, Bitsolution and contributors
// For license information, please see license.txt


frappe.ui.form.on('Work Order Maintenance', {
	// fields:[{
	// 	fieldname: "id_dailyBD",
	// 	label: __("id_dailyBD"),
	// 	fieldtype: "Data",
	// 	hidden:1
	// }],
	setup: function(frm){
		// frm.add_field('iddb',"");
	},
	refresh: function(frm) {
		frm.set_df_property("bd_auto_create_bd", "hidden", frm.is_new() ? 0 : 1);
		
		frm.set_df_property('bd_delay_hours', 'cannot_delete_rows', 1);
		frm.set_df_property('bd_delay_hours', 'cannot_add_rows', 1);

		frm.set_query('bd_subcomponent', function() {
			if(frm.doc.bd_component){
				return {
					filters: {
						'plan_component_id': frm.doc.bd_component,
					}
				};
			}else{
				return None;
			}
		});

		if(frm.is_new()){
			// set data table delay hourse
			frappe.db.get_list('BD Delay', {
				fields: ['name','bd_delay_abbr','bd_delay_desc'],
			}).then(function(data) {

				// set null table mechanic 
				frm.doc.bd_delay_hours = [];
				if (data){
					data.forEach(function(v) {

						// inject item to child table
						frm.add_child('bd_delay_hours', {
							dbd_dh_delay_id: v.name,
							dbd_dh_abbr: v.bd_delay_abbr,
							dbd_dh_desc: v.bd_delay_desc,
							dbd_dh_date: frm.doc.bd_report_date
						});
						
					});
					frm.refresh_field('bd_delay_hours');
				}
				
			});
		}

	},
	bd_unit_number: function(frm){
		frappe.call({
			async:false,
			method: "frappe.client.get_value",
			args: {
				doctype:'Summary Hours Meter',
				fieldname: ['name', 'summary_real_hm', 'summary_cur_hm'],
				filters:{summary_asset_hm: frm.doc.hm_replace_asset}
			},
			callback: function (r) {
				console.log(r);
				if (!r.exc) {
					let valReal=0;
					let valCur=0;
					if(r.message.summary_real_hm != null){
						valReal = r.message.summary_real_hm;
					}
					if(r.message.summary_cur_hm != null){
						valCur = r.message.summary_cur_hm;
					}
					frm.set_value("bd_real_hm", valReal);
					frm.set_value("bd_cur_hm", valCur);
				}
			}
		});
	},
	// dbd_dh_hours: function(frm){
	// 	console.log("dbd_dh_hours");
	// },
	before_save: function(frm){
		
		for (let row of frm.fields_dict.bd_mechanic.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'dbd_mech_date',frm.doc.bd_report_date);
			
		}

		// let calTotalHours = 0;
		for (let row of frm.fields_dict.bd_delay_hours.grid.grid_rows) {
			frappe.model.set_value(row.doc.doctype,row.doc.name,'dbd_dh_date',frm.doc.bd_report_date);
		// 	calTotalHours +=row.doc.dbd_dh_hours;
		}
		// frm.set_value("bd_delay_total_hours",calTotalHours);
		
		// if(frm.is_new() && frm.doc.bd_auto_create_bd ){ //frm.is_new() && 
		// 	console.log("if");
		// 	frappe.call({
		// 		method: "addons.plan.doctype.daylybd.daylybd.auto_create_daylybd",
		// 		args: {
		// 			data: frm.doc,
		// 		},
		// 		callback: function (r) {
		// 			console.log(r);
		// 			console.log(r.message.name);
		// 			if (!r.exc) {
		// 				// frm.set_value("id_dailyBD",r.message.name);
		// 				if (r.message) {
		// 					frappe.show_alert({
		// 						message: __("Auto create dayly breakdown success!"),
		// 						indicator: "blue",
		// 					});
		// 					// cur_dialog.hide();
		// 				}
		// 			}
		// 			console.log(iddb);
		// 		},
		// 		error: function(e){
		// 			console.log(e);
		// 		}
		// 	});
		// }
	},
	after_save: function(frm){
		console.log("is new after save");
		console.log(frm);
		if(frm.doc.bd_auto_create_bd ){ //frm.is_new() && 
			
			frappe.call({
				method: "addons.plan.doctype.daylybd.daylybd.auto_create_daylybd",
				args: {
					data: frm.doc,
				},
				callback: function (r) {
					if (!r.exc) {
						if (r.message) {
							frappe.show_alert({
								message: __("Auto create dayly breakdown success!"),
								indicator: "blue",
							});
							iddb = r.message.docname;
							// cur_dialog.hide();
						}
					}
					console.log(iddb);
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	}

});

frappe.ui.form.on('DaylyBD Delay Hours', {
	dbd_dh_hours: function(frm, cdt, cdn){
		console.log("dbd_dh_hours in child");
		let calTotalHours = 0;
		for (let row of frm.fields_dict.bd_delay_hours.grid.grid_rows) {
			calTotalHours +=row.doc.dbd_dh_hours;
		}
		frm.set_value("bd_delay_total_hours",calTotalHours);
	},
});