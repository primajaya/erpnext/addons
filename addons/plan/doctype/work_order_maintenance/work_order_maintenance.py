# Copyright (c) 2024, Bitsolution and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import json


from addons.utils.logger import get_logger

log = get_logger(__name__)

class WorkOrderMaintenance(Document):
	pass

@frappe.whitelist()
def update_data_wo_from_bd(data):
    if isinstance(data, str):
        data = json.loads(data)
    log.info(data)
    dataBD = frappe._dict(data)
    
    # get data wo
    dataWO = frappe.db.get("Work Order Maintenance",dataBD.bd_worker_order_number)
    
    # update mech wo if not in mech daily breakdown
    for bd_mechanic_bd in dataBD.bd_mechanic:
        bd_mechanic_bd = frappe._dict(bd_mechanic_bd)
        bd_mechanic_wo = frappe.db.get("DaylyBD Mechanic",{'employee_id':bd_mechanic_bd.employee_id,'parent':dataBD['bd_worker_order_number'],'parenttype':"Work Order Maintenance"})
       
        if (bd_mechanic_wo is None):
            frappe.get_doc(dict(
				doctype="DaylyBD Mechanic",
				employee_id=bd_mechanic_bd.employee_id,
				dbd_mech_name=bd_mechanic_bd.dbd_mech_name,
				dbd_mech_date=bd_mechanic_bd.dbd_mech_date,
				parenttype="Work Order Maintenance",
				parent=dataBD['bd_worker_order_number'],
				parentfield=bd_mechanic_bd.parentfield
			)).insert()
    
    # update delay hm WO
    # if create new doc
    updateDoc = True
    if(data.get('__islocal') and data['name'].find('new')>=0):
        log.info("new")
        updateDoc=False
        
    for bd_delay_hours_bd in dataBD.bd_delay_hours:
        bd_delay_hours_bd= frappe._dict(bd_delay_hours_bd)
        bd_delay_hours_wo = frappe.db.get("DaylyBD Delay Hours",{'dbd_dh_delay_id':bd_delay_hours_bd.dbd_dh_delay_id,'parent':dataBD['bd_worker_order_number'],'parenttype':"Work Order Maintenance"})
        
        if(updateDoc):
            bd_delay_hours_bd_old = frappe.db.get("DaylyBD Delay Hours",bd_delay_hours_bd.name)
            bd_delay_hours_wo.dbd_dh_hours -= bd_delay_hours_bd_old.dbd_dh_hours
            
        bd_delay_hours_wo.dbd_dh_hours += bd_delay_hours_bd.dbd_dh_hours
        frappe.db.set_value("DaylyBD Delay Hours",bd_delay_hours_wo.name,'dbd_dh_hours',bd_delay_hours_wo.dbd_dh_hours)
        # bd_delay_hours_wo.db_update()
        		
    # update data total delay wo
    if(updateDoc):
        dataBDOld = frappe.db.get("DaylyBD",dataBD.name)
        dataWO.bd_delay_total_hours -= dataBDOld.bd_delay_total_hours
  
    dataWO.bd_delay_total_hours += dataBD.bd_delay_total_hours
    frappe.db.set_value("Work Order Maintenance",dataWO.name,'bd_delay_total_hours',dataWO.bd_delay_total_hours)
    # dataWO.db_update()
    
    
