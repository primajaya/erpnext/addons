frappe.pages['plan-periodic-svc-ra'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Plan Periodic By Range Date',
		single_column: true
	});

	page.add_label('Period Date');

	page.period_date_field = page.add_field({
		fieldname: 'period_date',
		label: 'Period Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	page.add_label('Start Date');

	page.start_date_field = page.add_field({
		fieldname: 'start_date',
		label: 'Start Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	page.add_label('End Date');

	page.end_date_field = page.add_field({
		fieldname: 'end_date',
		label: 'End Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	let list = $(frappe.render_template('<div class="table-area"></div>')).appendTo(page.main);

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.plan.page.plan_periodic_svc_ra.plan_periodic_svc_ra.get_data',
			// typr:'GET',
			args: {
				period_date:page.period_date_field.get_value(),
				start_date:page.start_date_field.get_value(),
				end_date:page.end_date_field.get_value()
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					make_table(r.message);
				}
			}
		});
	}

	
	function make_table(data){
		list.html("");
		$(frappe.render_template("plan_periodic_svc_ra",{data:data})).appendTo(list);
	}

}