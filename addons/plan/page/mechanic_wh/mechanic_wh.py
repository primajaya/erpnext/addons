import frappe
from frappe import _
from datetime import date, datetime, timedelta

from addons.utils.logger import get_logger


log = get_logger(__name__)

@frappe.whitelist()
def get_data_by_date_range(
      start_date=date.today(),
      end_date=date.today(),
      ):
    if isinstance(start_date, str):
        start_date = datetime.strptime(start_date,'%Y-%m-%d').date()
    if isinstance(end_date, str):
        end_date = datetime.strptime(end_date,'%Y-%m-%d').date()

    if(end_date < start_date):
        frappe.throw(_("End date cant below start date"))
        
    data = {'header':['NRP','Name','Position']}
    
    # headers = frappe.db.get_list("Plan Component", pluck='plan_comp_name')
    headers = frappe.db.get_list("Plan Component", fields=['name', 'plan_comp_name'])
    
    
    where = """ where 1=1 """
    values = {}
    groupBy = """ """
    joining=""" """
    
    values['start_date'] = start_date.strftime('%Y-%m-%d') 
    values['end_date'] = end_date.strftime('%Y-%m-%d')
    
    field = """
            e.name, e.employee_name, e.designation 
            """
    fromtbl = """ from tabEmployee as e """
    
    field2 = """ a.* """
    field2TotalPLanComp = """, ( """
    for i,header in enumerate(headers):
        # log.info(header)
        if(i>0):
            field2TotalPLanComp += """ + """
        fieldHeaderAs = header.plan_comp_name.lower().replace(" ","_")+"_"+header.name
        # values[fieldHeaderAs] = header.name
        (data['header']).append(header.plan_comp_name)
        field += """
                ,COALESCE((
                    SELECT SUM(twom.bd_delay_total_hours)
                    FROM `tabWork Order Maintenance` AS twom 
                    left join `tabDaylyBD Mechanic` AS tdbm ON tdbm.parent = twom.name AND tdbm.parenttype ="Work Order Maintenance" 
                    WHERE 
                        twom.bd_report_date BETWEEN %(start_date)s AND  %(end_date)s AND 
                        twom.bd_component = '"""+header.name+"""'
                        and tdbm.employee_id = e.name
                         
                    GROUP BY tdbm.employee_id, twom.bd_component
                ),0) as 
                """+fieldHeaderAs+""" """
        # -- twom.bd_component = %("""+fieldHeaderAs+""")s AND 
        field2TotalPLanComp +="""a."""+fieldHeaderAs
        
    field2TotalPLanComp += """ ) as  total_plan_comp """
    field2 += field2TotalPLanComp
    
    (data['header']).append("Total")
    (data['header']).append("Efective %")
    (data['header']).append("Status") # status efective perform/not perform (>= 60% perform)
    
    
    query="""
        select 
        """+field+fromtbl+joining+where+groupBy+"""

    """ 
    
    query2 = """ SELECT """+field2+""" FROM ("""+query+""") as a"""
    
    # (SELECT SUM(sum_q3_1.total_plan_comp) FROM """+query2+""" as  sum_q3_1)/
    
    field3Efective =""" COALESCE(
                q3.total_plan_comp / (
                    (SELECT SUM(sum_q3_1.total_plan_comp) FROM ("""+query2+""") as sum_q3_1)/
                    (
                        (SELECT COUNT(count_q3_1.employee_name) FROM ("""+query+""") as count_q3_1) -
                        (SELECT SUM(CASE WHEN count_q3_2.total_plan_comp=0 THEN 1 ELSE 0 END) FROM ("""+query2+""") as count_q3_2)
                    )
                ), 
                0
            )*100"""
    field3 = """ q3.*, 
            """+field3Efective+""" AS efective,
            CASE WHEN """+field3Efective+"""<60 THEN 'NOT PERFORM' ELSE 'PERFORM' END AS status_efective
            """
    query3 = """ SELECT """+field3+""" FROM ("""+query2+""") as q3"""
    
    
    
    # print(query)
    # frappe.msgprint(query3)
    content = frappe.db.sql( query=query3, values=values ,as_dict=False)


    data['content'] = content
    
    log.info(content)
    
    return data
    
        