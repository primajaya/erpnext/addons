frappe.pages['mechanic-wh'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Mechanic Working Hours ',
		single_column: true
	});


	page.start_date_field = page.add_field({
		fieldname: 'start_date',
		label: 'Start Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	page.end_date_field = page.add_field({
		fieldname: 'end_date',
		label: 'End Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});


	let list = $(frappe.render_template('<div class="table-area"></div>')).appendTo(page.main);

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.plan.page.mechanic_wh.mechanic_wh.get_data_by_date_range',
			args: {
				start_date:page.start_date_field.get_value(),
				end_date:page.end_date_field.get_value()
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					make_table(r.message);
				}
			}
		});
	}

	
	function make_table(data){
		list.html("");
		$(frappe.render_template("mechanic_wh",{data:data})).appendTo(list);
	}
}