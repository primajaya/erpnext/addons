import frappe
import math
from frappe import _
from datetime import date, datetime, timedelta


from addons.utils.logger import get_logger


log = get_logger(__name__)

@frappe.whitelist()
def get_data(
    period_date=date.today(),
    danger_color='#e24c4c',
	danger_range=4,
	warning_color='#f8814f',
	warning_range=7
      ):
    if isinstance(period_date, str):
        period_date = datetime.strptime(period_date,'%Y-%m-%d').date()
        
    rowspanHeader = 2
    
    data = {'header':[]}
    
    tableHeader = [
        {'name':'Unit Number','rowspan':rowspanHeader,'colspan':0}, 
        {'name':'Name Asset','rowspan':rowspanHeader,'colspan':0}, 
        {'name':'Name Series Asset','rowspan':rowspanHeader,'colspan':0},
        {'name':'Item Code','rowspan':rowspanHeader,'colspan':0},
        {'name':'Branch','rowspan':rowspanHeader,'colspan':0},
        {'name':'AVG HM Opration','rowspan':rowspanHeader,'colspan':0},
        {'name':'Interval Service','rowspan':rowspanHeader,'colspan':0},
        {'name':'Last HM','rowspan':rowspanHeader,'colspan':0},
        ]
    
    where = """ where 1=1 """
    values = {}
    groupBy = """ """
    joining=""" """
    
    values['period_date'] = period_date.strftime('%Y-%m-%d') 
    
    field = """
            tpm.pm_asset, 
            tpm.pm_asset_name, 
            tpm.pm_asset_series, 
            tpm.pm_asset_item_code, 
            tpm.pm_asset_branch, 
            tpm.pm_avg_hm_opr, 
            tpm.pm_interval_service, 
            
            """
            # tpm.pm_last_hm, 
    # fieldLastHM =""" (
    #   SELECT hm.plan_hm_real 
	# 	FROM (
	# 		SELECT max(thm.plan_hm_date),thm.plan_hm_real, thm.plan_hm_asset 
	# 		FROM `tabHours Meter` thm 
	# 		WHERE thm.plan_hm_date < %(period_date)s
	# 		GROUP BY thm.plan_hm_asset
	# 		ORDER BY plan_hm_date DESC
	# 		) as hm 
	# 	WHERE hm.plan_hm_asset=tpm.pm_asset
	# 	) """
    fieldLastHM = """ ( SELECT hm.summary_real_hm FROM `tabSummary Hours Meter` as hm WHERE hm.summary_asset_hm = tpm.pm_asset ) """
    field += fieldLastHM +""" as pm_last_hm_sub, """
    fromtbl = """ from `tabPlan Monitoring` as tpm """
    
    tableHeader2 = []
    orderByDate = """ """
    for i in range(4) :
        fieldInFor = """ """
        if(i>0):
            fieldInFor += """, """
            orderByDate += """, """
            
        tableHeader.append({'name':'Service '+str(i+1),'rowspan':0,'colspan':2})
        # row2 = [{'name':'Date','rowspan':0,'colspan':0},{'name':'HM','rowspan':0,'colspan':0}]
        
        tableHeader2.append({'name':'HM','rowspan':0,'colspan':0})
        tableHeader2.append({'name':'Date','rowspan':0,'colspan':0})
        tableHeader.append({'name':'SVC Type','rowspan':rowspanHeader,'colspan':0})
        
        hm_service = """ hm_service"""+str(i+1)+""" """
        date_service = """ date_service"""+str(i+1)+""" """
        type_service = """ type_service"""+str(i+1)+""" """
        
        orderByDate +="""q.date_service"""+str(i+1)+""" ASC """
        
        # WHEN tpm.pm_last_hm IS NOT NULL THEN (FLOOR(tpm.pm_last_hm/ tpm.pm_interval_service)+1) * tpm.pm_interval_service
        fieldhm = """ 
                CASE
                    
                    WHEN """+fieldLastHM+""" IS NOT NULL THEN (FLOOR("""+fieldLastHM+"""/ tpm.pm_interval_service)+1) * tpm.pm_interval_service
                    ELSE 0
                END 
                """
        if(i>0):
            fieldhm = factorial_Sql_hm_service(fieldhm,i)
            
        fieldInFor += fieldhm+""" AS """+hm_service+""", """
        
        # WHEN tpm.pm_last_hm IS NOT NULL THEN DATE_ADD(%(period_date)s, INTERVAL ("""+fieldhm+""" - tpm.pm_last_hm) / tpm.pm_avg_hm_opr DAY )
        fieldInFor +="""
                CASE 
                    
                    WHEN """+fieldLastHM+""" IS NOT NULL THEN DATE_ADD(%(period_date)s, INTERVAL ("""+fieldhm+""" - """+fieldLastHM+""") / tpm.pm_avg_hm_opr DAY )
                    ELSE NULL
                END AS """+date_service+""", """
                
        fieldInFor +=""" 
                CASE 
                    WHEN MOD("""+fieldhm+""", 4000) = 0 AND MOD("""+fieldhm+""", 2000) = 0 AND MOD("""+fieldhm+""", 500) = 0 THEN '5'
                    WHEN MOD("""+fieldhm+""", 2000) = 0 AND MOD("""+fieldhm+""", 500) = 0 THEN '4'
                    WHEN MOD("""+fieldhm+""", 1000) = 0 AND MOD("""+fieldhm+""", 500) = 0 THEN '3'
                    WHEN MOD("""+fieldhm+""", 500) = 0 THEN '2'
                    ELSE '1'
                END AS """+type_service+""" """
                
        field += fieldInFor
                
    field2 = """ 
            q.*, 
            
            """
    fieldTotalES = """ """
    
    for i in range(5):
        field2ES = """  """
        for j in range(4):
            type_service = """ """
            if(j>0):
                type_service += """ + """
                
            type_service += """ (CASE WHEN q.type_service"""+str(j+1)+"""="""+str(i+1)+""" THEN 1 ELSE 0 END) """
            field2ES +=type_service
            
        
        # log.info("==================================== ")
        # log.info(field2ES)
        
        if(i>0):
            fieldTotalES += """ + """
        fieldTotalES +=field2ES 
        
        field2 += """("""+field2ES +""") AS es"""+str(i+1)+""", """
        
    field2 += fieldTotalES +""" AS total_es """
        
    tableHeader.append({'name':'Service Event','rowspan':0,'colspan':5})
    tableHeader2.append({'name':'I 250','rowspan':0,'colspan':0})
    tableHeader2.append({'name':'II 500','rowspan':0,'colspan':0})
    tableHeader2.append({'name':'III 1000','rowspan':0,'colspan':0})
    tableHeader2.append({'name':'IV 2000','rowspan':0,'colspan':0})
    tableHeader2.append({'name':'V 4000','rowspan':0,'colspan':0})
    
    tableHeader.append({'name':'Total Event','rowspan':rowspanHeader,'colspan':0})
    # tableHeader.append({'name':'Warranty Stats','rowspan':rowspanHeader,'colspan':0})
    
    (data['header']).append(tableHeader)
    (data['header']).append(tableHeader2)
    
    query=""" select """+field+fromtbl+joining+where+groupBy+""" """ 
    
    query2 = """ select """+field2+""" from ( """+query+""" ) as q ORDER BY """+orderByDate
    # query2 = """ select q.* from ( """+query+""" ) as q"""
    # print(query2)
    # frappe.msgprint(query2)
    contents = frappe.db.sql( query=query2, values=values ,as_dict=True)
    contentsBackground =[]
    
    log.info(contents)
    
    for content in contents:
        contentBackground = []
        for key, value in content.items():
            if (key.find('date_service')>=0):
                val = {'value':value,'color':''}
                
                
                if(date.today() <= datetime.strptime(value, '%Y-%m-%d').date() <= date.today() + timedelta(float(danger_range))):
                    val['background'] = danger_color
                elif(date.today() <= datetime.strptime(value, '%Y-%m-%d').date() <= date.today() + timedelta(float(warning_range))):
                    val['background'] = warning_color
                
                log.info(val)
                log.info(date.today())
                log.info(date.today() - timedelta(float(warning_range)))
                log.info(date.today() - timedelta(float(danger_range)))
                contentBackground.append(val)
                
                # contentBackground.append({'value':value,'color':'Red','background':'#fff5f5'}) #e24c4c
                # contentBackground.append({'value':value,'color':'Orange','background':'#fff5f0'}) #f8814f
            else:
                contentBackground.append({'value':value,'color':'','background':''})
        
        contentsBackground.append(contentBackground)
            
    data['content'] = contentsBackground
    
    log.info(contentsBackground)
    
    return data
    

def factorial_Sql_hm_service(field,n):
    if(n>1):
        n-=1
        field = factorial_Sql_hm_service(field,n)
    
    return """ 
            CASE 
                WHEN  """+field+"""= 0 THEN NULL
                ELSE """+field+""" + tpm.pm_interval_service
            END
            """