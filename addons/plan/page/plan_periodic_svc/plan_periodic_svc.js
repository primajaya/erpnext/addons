frappe.pages['plan-periodic-svc'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Plan Periodic Service',
		single_column: true
	});


	page.period_date_field = page.add_field({
		fieldname: 'period_date',
		label: 'Period Date',
		fieldtype:'Date',
		default: frappe.datetime.get_today(),
		change: function() {
			get_data();
		}
	});

	page.danger_color_field = page.add_field({
		fieldname: 'danger_color',
		label: 'Danger Color',
		fieldtype:'Color',
		default: '#e24c4c',
		change: function() {
			get_data();
		}
	});

	page.danger_range_field = page.add_field({
		fieldname: 'danger_range',
		label: 'Danger Range',
		fieldtype:'Int',
		default: '4',
		change: function() {
			get_data();
		}
	});

	page.warning_color_field = page.add_field({
		fieldname: 'warning_color',
		label: 'Warning Color',
		fieldtype:'Color',
		default: '#f8814f',
		change: function() {
			get_data();
		}
	});

	page.warning_range_field = page.add_field({
		fieldname: 'warning_range',
		label: 'Warning Range',
		fieldtype:'Int',
		default: '7',
		change: function() {
			get_data();
		}
	});

	let list = $(frappe.render_template('<div class="table-area"></div>')).appendTo(page.main);

	function get_data(){
		let me = this
		frappe.call({
			method:'addons.plan.page.plan_periodic_svc.plan_periodic_svc.get_data',
			// typr:'GET',
			args: {
				period_date:page.period_date_field.get_value(),
				danger_color:page.danger_color_field.get_value(),
				danger_range:page.danger_range_field.get_value(),
				warning_color:page.warning_color_field.get_value(),
				warning_range:page.warning_range_field.get_value(),
				
			},
			callback: function(r) {
	
				if(!r.exc) {
					
					make_table(r.message);
				}
			}
		});
	}

	
	function make_table(data){
		list.html("");
		$(frappe.render_template("plan_periodic_svc",{data:data})).appendTo(list);
	}
}