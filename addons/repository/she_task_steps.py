import frappe
from frappe import _

def get_child_table_task_steps(designation):
    sts = frappe.qb.DocType("SHE Task Steps")
    shc = frappe.qb.DocType("SHE Hazard Category")
    
    return (
        frappe.qb.from_(sts).as_('sts')
            .left_join(shc).on(shc.name == sts.she_hazard_category_id)
            .select(
                sts.name,
                sts.task_step_name,
                sts.she_hazard_category_id,
                shc.hazard_name,
                shc.hazard_code
            )
            .where(sts.designation == designation)
    ).run(as_dict=True)