import frappe
from frappe import _

def get_data_by_parent(designation):
    stsd = frappe.qb.DocType("SHE Task Steps Detail")
    
    return (
        frappe.qb.from_(stsd).as_('sts')
            .select(
                stsd.name,
                stsd.task_step_name,
                stsd.she_hazard_category_id,
                stsd.she_hazard_category_code,
            )
            .where(stsd.parent == designation)
    ).run(as_dict=True)