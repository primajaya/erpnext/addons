import frappe
from frappe import _
from datetime import datetime

from frappe.utils.data import getdate


def get_count_filled_room(room_id=None, start_date=None, end_date=None ):
    where = """ """
    values = {}
    if room_id:
        where += """ and fill.dormitory_room_id=%(room_id)s """
        values['room_id']=room_id
    if start_date and end_date :
        days = (getdate(end_date)-getdate(start_date)).days+1
        if days == 1 :
            where += """ and fill.resident_date between %(start_date)s and %(end_date)s """
        values['start_date']=start_date
        values['end_date']=end_date

    sql_str = """
        select count(*) as count_filled , fill.dormitory_room_id, fill.resident_date
        from 
        (
            select 
            max(a.name) as name
            ,max(a.type_log) as type_log
            ,a.dormitory_room_id
            ,resident_date
            from `tabDormitory Resident` as a
            group by employee_id, resident_date
            ORDER BY a.name DESC
        ) as fill
        where fill.type_log ='IN' """+where+"""
        group by fill.resident_date, fill.dormitory_room_id
        order by count(*) DESC
        """
    
    if start_date and end_date :
        days = (getdate(end_date)-getdate(start_date)).days+1
        if days > 1 :
            sql_str = """
                select max(fill2.count_filled) as count_filled, fill2.dormitory_room_id, fill2.resident_date
                from ( """+ sql_str +""" ) as fill2 
                where fill2.resident_date between %(start_date)s and %(end_date)s 
                group by fill2.dormitory_room_id 
                """

    return frappe.db.sql(
        sql_str,
        values=values,
        as_dict=True)