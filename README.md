# Addons

## Introduction

Addons erp primajaya

## Installation

1. [Install bench](https://gitlab.com/primajaya/erpnext/frappe_docker).
2. [Install ERPNext](https://gitlab.com/primajaya/erpnext/erpnext).
3. [Install HRMS](https://gitlab.com/primajaya/erpnext/hrms).
4. Once ERPNext is installed, add the hrms app to your bench by running

	```sh
	bench get-app --branch main --resolve-deps https://gitlab.com/primajaya/erpnext/addons
	```
5. After that, you can install the hrms app on the required site by running
	```sh
	bench --site sitename install-app addons
	```

## Pull and Push
before commit and push to repository, if you update on database doctype or something
  ```sh 
  bench --site sitename export-fixtures
  ```

you can commit n push to repository

if you wont pull from your git repository and update app on your site, first go to dir your app and pull, after that, go to dir your workingspace and update your site

```sh
bench --site sitename migrate
```


## License

MIT
